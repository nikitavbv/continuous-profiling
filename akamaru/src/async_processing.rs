use {
    profiles_processing_job::run_profiles_processing_job,
    crate::component::AkamaruComponent,
};

pub struct ProfilesProcessingJobComponent {
}

impl AkamaruComponent for ProfilesProcessingJobComponent {
    fn run() {
        run_profiles_processing_job()
    }
}