use {
    std::{env, collections::HashMap, thread::spawn},
    log::{info, error},
    env_logger::Env,
    continuous_profiler_agent::ContinuousProfiler,
    profiles_processing_job::{run_profiles_processing_job, run_perf_counters_processing_job},
    symbols_processing_job::run_symbols_processing_job,
    crate::{component::AkamaruComponent, async_processing::ProfilesProcessingJobComponent},
};

mod async_processing;
mod component;

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    println!(r#"                                                         
       ,--.                                             
,--,--.|  |,-. ,--,--.,--,--,--. ,--,--.,--.--.,--.,--. 
' ,-.  ||     /' ,-.  ||        |' ,-.  ||  .--'|  ||  | 
\ '-'  ||  \  \\ '-'  ||  |  |  |\ '-'  ||  |   '  ''  ' 
 `--`--'`--'`--'`--`--'`--`--`--' `--`--'`--'    `----'  
"#);

    run_profiler();

    let profiles_processing_job = spawn(|| ProfilesProcessingJobComponent::run());
    let perf_counters_processing_job = spawn(|| run_perf_counters_processing_job());
    let symbols_processing_job = spawn(|| run_symbols_processing_job());

    profiles_processing_job.join().unwrap();
    perf_counters_processing_job.join().unwrap();
    symbols_processing_job.join().unwrap();
}

fn run_profiler() {
    ContinuousProfiler::new(
        env::var("CONTINUOUS_PROFILER_ENDPOINT").unwrap_or("profiling-coordinator.default.svc.cluster.local:7878".to_owned()),
        "akamaru".to_owned()
    )
        .with_tags({
            let mut tags = HashMap::new();
                tags.insert("env".to_owned(), env::var("ENV").unwrap_or("unknown".to_owned()));
                tags
            })
        .with_access_token("replace-later-with-real-token".to_owned())
        .run();
}