#![recursion_limit = "1024"]

use {
    std::{thread, time::Duration, env, io::Cursor},
    log::{info, error},
    kafka::{consumer::Consumer, client::{FetchOffset, GroupOffsetStorage}},
    profiling_core::{
        protocol::{
            KAFKA_AGGREGATIONS_CALCULATION_JOB_CONSUMER_GROUP,
            KAFKA_TOPIC_COLLECTED_PERF_COUNTERS,
            KAFKA_PERF_COUNTERS_PROCESSING_JOB_CONSUMER_GROUP,
            CollectedProfileWithMetadata,
            Flamegraph,
            AgentMetadata,
        },
        perf_counters::PerfCounterData,
        state::ProfilingState,
    },
};

const KAFKA_TOPIC_COLLECTED_PROFILES: &str = "collected-profiles";

const NO_PROFILES_TO_PROCESS_SLEEP_DELAY: Duration = Duration::from_secs(10);
const CONSUMER_CREATION_RETRY_INTERVAL: Duration = Duration::from_secs(5);
const CONSUMER_FAIL_RETRY_INTERVAL: Duration = Duration::from_secs(5);

pub fn run_perf_counters_processing_job() {
    info!("running perf counters processing job");
    let mut consumer_maybe = None;

    loop {
        if consumer_maybe.is_none() {
            consumer_maybe = Some(match Consumer::from_hosts(kafka_broker_hosts())
                .with_topic(KAFKA_TOPIC_COLLECTED_PERF_COUNTERS.to_owned())
                .with_group(KAFKA_PERF_COUNTERS_PROCESSING_JOB_CONSUMER_GROUP.to_owned())
                .with_fallback_offset(FetchOffset::Earliest)
                .with_offset_storage(GroupOffsetStorage::Kafka)
                .create() {
                    Ok(v) => v,
                    Err(err) => {
                        error!("failed to create consumer: {}", err);
                        thread::sleep(CONSUMER_CREATION_RETRY_INTERVAL);
                        continue;
                    }
                }
            );
        }

        let consumer = consumer_maybe.as_mut().unwrap();

        let msgss = match consumer.poll() {
            Ok(v) => v,
            Err(err) => {
                error!("failed to poll consumer: {:?}", err);
                consumer_maybe = None;
                thread::sleep(CONSUMER_FAIL_RETRY_INTERVAL);
                continue;
            }
        };

        if msgss.is_empty() {
            info!("no perf counter events to process at this time");
            thread::sleep(NO_PROFILES_TO_PROCESS_SLEEP_DELAY);
            continue;
        }

        for msgs in msgss.iter() {
            for message in msgs.messages() {
                info!("new perf counter event message");
                let (perf_counter_data, agent_metadata): (PerfCounterData, AgentMetadata) = serde_json::from_slice(message.value).unwrap();
                info!("perf counter: {:?}, metadata: {:?}", perf_counter_data, agent_metadata);
            }
            consumer.consume_messageset(msgs).unwrap();
        }
        consumer.commit_consumed().unwrap();
    }
}

pub fn run_profiles_processing_job() {
    info!("running profiles processing job");

    let mut consumer_maybe = None;

    let mut state = ProfilingState::new();

    loop {
        if consumer_maybe.is_none() {
            consumer_maybe = Some(match Consumer::from_hosts(kafka_broker_hosts())
                    .with_topic(KAFKA_TOPIC_COLLECTED_PROFILES.to_owned())
                    .with_group(KAFKA_AGGREGATIONS_CALCULATION_JOB_CONSUMER_GROUP.to_owned())
                    .with_fallback_offset(FetchOffset::Earliest)
                    .with_offset_storage(GroupOffsetStorage::Kafka)
                    .create() {
                        Ok(v) => v,
                        Err(err) => {
                            error!("failed to create consumer: {}", err);
                            thread::sleep(CONSUMER_CREATION_RETRY_INTERVAL);
                            continue;
                        }
                    }
            );
        }

        let consumer = consumer_maybe.as_mut().unwrap();

        let msgss = match consumer.poll() {
            Ok(v) => v,
            Err(err) => {
                error!("failed to poll consumer: {:?}", err);
                consumer_maybe = None;
                thread::sleep(CONSUMER_FAIL_RETRY_INTERVAL);
                continue;
            }
        };

        if msgss.is_empty() {
            info!("no profiles to process at this time");
            thread::sleep(NO_PROFILES_TO_PROCESS_SLEEP_DELAY);
            continue;
        }

        for msgs in msgss.iter() {
            for message in msgs.messages() {
                let collected_profile_json: Vec<u8> = zstd::stream::decode_all(Cursor::new(message.value)).unwrap();
                let collected_profile: CollectedProfileWithMetadata = serde_json::from_slice(&collected_profile_json).unwrap();
                
                let service_name = &collected_profile.service_name;
                let service_version = &collected_profile.service_version;
                let frames_count = collected_profile.collected_profile.entries.iter()
                    .map(|entry| entry.count) 
                    .sum::<u32>();
                state.create_service_if_not_exists(service_name);
                state.create_service_version_if_not_exists(service_name, service_version);
                state.update_recent_versions(
                    service_name, 
                    service_version, 
                    collected_profile.collected_profile.params.started_at.clone(), 
                    frames_count
                );
                state.update_recent_activity(
                    service_name,
                    collected_profile.collected_profile.params.started_at.clone(),
                    frames_count
                );
                state.save_latest_profile_for_service(service_name, &collected_profile.collected_profile);
                
                // add to flamegraph
                // equivalent to query:
                // input: CollectedProfile
                // operator 0: flamegraph_from_collected_profile
                // operator 1: merge_flamegraphs
                // output: Flamegraph
                let new_flamegraph = Flamegraph::from_collected_profile(&collected_profile.collected_profile);
                let existing_flamegraph = state.flamegraph_for_service_version(service_name, service_version);
                let mut flamegraph = existing_flamegraph.map(|v| v.merge(&new_flamegraph)).unwrap_or(new_flamegraph);

                if let Some(symbols) = state.symbols_for_service_version(service_name, service_version) {
                    flamegraph.enrich_with_symbols(&symbols);
                }

                state.save_flamegraph(service_name, service_version, &flamegraph);

                // add to top functions
                // equivalent to query:
                // input: CollectedProfile
                // operator 0: extract_top_functions
                // operator 1: combine_top_functions
                // output: TopFunctions
                //let new_top_functions = TopFunctions::from_collected_profile(&collected_profile.collected_profile);
                //let existing_top_functions = state.top_functions_for_service_version(service_name, service_version);
                //let mut top_functions = existing_top_functions.map(|v| v.merge(&new_top_functions)).unwrap_or(new_top_functions);
                //state.save_top_functions(service_name, service_version, &flamegraph);

                info!("processed profile for {} version {}, frames count: {:?}", service_name, service_version, frames_count);
            }
            consumer.consume_messageset(msgs).unwrap();
        }
        consumer.commit_consumed().unwrap();
    }
}

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING").unwrap_or("redis://127.0.0.1:6379/".to_owned())
}

pub fn kafka_broker_hosts() -> Vec<String> {
    vec![env::var("BROKER_HOST").unwrap_or("127.0.0.1:9092".to_owned())]
}