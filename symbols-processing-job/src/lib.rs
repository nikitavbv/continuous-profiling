use {
    std::{env, collections::HashMap, time::Duration, thread, rc::Rc},
    env_logger::Env,
    log::{info, error},
    kafka::{consumer::Consumer, client::{FetchOffset, GroupOffsetStorage}},
    s3::{Region, bucket::Bucket, creds::Credentials},
    redis::Commands,
    addr2line::gimli::{
        self, 
        EntriesTreeNode, 
        EndianReader, 
        RunTimeEndian, 
        Unit, 
        Dwarf,
        Reader,
        read::AttributeValue,
    },
    continuous_profiler_agent::ContinuousProfiler,
    profiling_core::{
        io::compress_symbols,
        protocol::{
            REDIS_KEY_PREFIX,
            KAFKA_TOPIC_SERVICE_BINARIES, 
            KAFKA_SYMBOLS_PROCESSING_JOB_CONSUMER_GROUP,
            ServiceBinaryUpdateEvent,
            ServiceSymbols,
            ServiceBinaryFunction,
            ServiceBinaryNamespace,
        },
        state::ProfilingState,
    },
};

const NO_SYMBOLS_TO_PROCESS_SLEEP_DELAY: Duration = Duration::from_secs(10);
const CONSUMER_CREATION_RETRY_INTERVAL: Duration = Duration::from_secs(5);
const CONSUMER_FAIL_RETRY_INTERVAL: Duration = Duration::from_secs(5);
const COMMIT_CONSUMED_RETRIES: u32 = 5;
const COMMIT_CONSUMED_RETRY_INTERVAL: Duration = Duration::from_secs(5);

struct ProcessedSymbols {
    functions: Vec<Function>,
}

#[derive(Clone)]
struct Namespace {
    name: Option<String>,
    parent: Option<Box<Namespace>>,
}

struct Function {
    namespace: Namespace,
    name: String,
    directory_name: Option<String>,
    file_name: Option<String>,
    line: Option<u64>,
}

impl ProcessedSymbols {
    pub fn new() -> Self {
        Self {
            functions: Vec::new(),
        }
    }

    pub fn to_service_symbols(&self) -> ServiceSymbols {
        ServiceSymbols {
            functions: self.functions.iter().map(|v| v.to_service_binary_function()).collect(),
        }
    }

    pub fn add_function(&mut self, function: Function) {
        self.functions.push(function);
    }
}

impl Namespace {
    pub fn empty() -> Self {
        Self {
            name: None,
            parent: None,
        }
    }

    pub fn child_with_name(self, name: String) -> Self {
        Self {
            name: Some(name),
            parent: Some(Box::new(self)),
        }
    }

    pub fn to_service_binary_namespace(&self) -> Option<ServiceBinaryNamespace> {
        self.name.as_ref().map(|name| ServiceBinaryNamespace {
            name: name.clone(),
            parent: self.parent.as_ref().and_then(|v| v.to_service_binary_namespace()).map(Box::new),
        })
    }
}

impl Function {
    pub fn new(namespace: Namespace, name: String, directory_name: Option<String>, file_name: Option<String>, line: Option<u64>) -> Self {
        Self {
            namespace,
            name,
            directory_name,
            file_name,
            line,
        }
    }

    pub fn to_service_binary_function(&self) -> ServiceBinaryFunction {
        ServiceBinaryFunction {
            namespace: self.namespace.to_service_binary_namespace(),
            name: self.name.clone(),
            directory_name: self.directory_name.clone(),
            file_name: self.file_name.clone(),
            line: self.line,
        }
    }
}

pub fn run_symbols_processing_job() {
    info!("running symbols processing job");

    ContinuousProfiler::new(
        env::var("CONTINUOUS_PROFILER_ENDPOINT").unwrap_or("profiling-coordinator.default.svc.cluster.local:7878".to_owned()),
        "symbols-processing-job".to_owned()
    )
        .with_tags({
            let mut tags = HashMap::new();
            tags.insert("env".to_owned(), env::var("ENV").unwrap_or("unknown".to_owned()));
            tags
        })
        .run();

    let mut consumer_maybe = None;

    let storage = Bucket::new(
        &env::var("STORAGE_BUCKET").unwrap_or("nikitavbv-continuous-profiling".to_owned()),
        Region::Custom {
            region: env::var("STORAGE_REGION").unwrap_or("europe-central2".to_owned()),
            endpoint: env::var("STORAGE_ENDPOINT").unwrap_or("https://storage.googleapis.com".to_owned()),
        },
        Credentials::from_env_specific(
            Some("STORAGE_ACCESS_KEY_ID"),
            Some("STORAGE_SECRET_ACCESS_KEY"),
            None,
            None,
        ).unwrap()
    ).unwrap();

    let mut state = ProfilingState::new();

    loop {
        if consumer_maybe.is_none() {
            consumer_maybe = Some(match Consumer::from_hosts(vec![kafka_broker_host()])
                .with_topic(KAFKA_TOPIC_SERVICE_BINARIES.to_owned())
                .with_group(KAFKA_SYMBOLS_PROCESSING_JOB_CONSUMER_GROUP.to_owned())
                .with_fallback_offset(FetchOffset::Earliest)
                .with_offset_storage(GroupOffsetStorage::Kafka)
                .create() {
                    Ok(v) => v,
                    Err(err) => {
                        error!("failed to create consumer: {:?}", err);
                        thread::sleep(CONSUMER_CREATION_RETRY_INTERVAL);
                        continue;
                    }
                }
            );
        }

        let consumer = consumer_maybe.as_mut().unwrap();

        let msgss = match consumer.poll() {
            Ok(v) => v,
            Err(err) => {
                error!("failed to poll consumer: {:?}", err);
                consumer_maybe = None;
                thread::sleep(CONSUMER_FAIL_RETRY_INTERVAL);
                continue;
            }
        };
        if msgss.is_empty() {
            info!("no symbols to process at this time");
            thread::sleep(NO_SYMBOLS_TO_PROCESS_SLEEP_DELAY);
            continue;
        }

        for msgs in msgss.iter() {
            for message in msgs.messages() {
                let message: ServiceBinaryUpdateEvent = serde_json::from_slice(&message.value).unwrap();
                info!("processing binary for service {} version {}", message.service, message.version);

                let service_binary_path = format!("services/{}/versions/{}/binary", message.service, message.version);
                let (service_binary, _) = storage.get_object_blocking(service_binary_path).unwrap();

                let processed_symbols = process_service_binary(&message.service, &message.version, &service_binary);
                let service_symbols = processed_symbols.to_service_symbols();
                state.update_service_symbols(&message.service, &message.version, &service_symbols);
                
                if let Some(mut flamegraph) = state.flamegraph_for_service_version(&message.service, &message.version) {
                    flamegraph.enrich_with_symbols(&service_symbols);
                    state.save_flamegraph(&message.service, &message.version, &flamegraph);
                }

                info!("done processing symbols for service {} version {}", message.service, message.version);
            }
            consumer.consume_messageset(msgs).unwrap();
        }

        let mut committed_consumed = false;
        for _ in 0..COMMIT_CONSUMED_RETRIES {
            if let Err(err) = consumer.commit_consumed() {
                error!("failed to commit consumed: {:?}", err); 
                thread::sleep(COMMIT_CONSUMED_RETRY_INTERVAL);
            } else {
                committed_consumed = true;
                break;
            }
        }
        if !committed_consumed {
            panic!("failed to commit consumed after {} retries", COMMIT_CONSUMED_RETRIES);
        }
    }
}

fn process_service_binary(service: &str, version: &str, service_binary: &[u8]) -> ProcessedSymbols {
    let file = addr2line::object::read::File::parse(service_binary).unwrap();
    let ctx = addr2line::Context::new(&file).unwrap();
    let dwarf = ctx.dwarf();

    let mut processed_symbols = ProcessedSymbols::new();

    let mut iter = dwarf.units();
    while let Some(header) = iter.next().unwrap() {
        let unit = dwarf.unit(header).unwrap();

        let mut tree = unit.entries_tree(None).unwrap();
        process_tree(&mut processed_symbols, &dwarf, &unit, tree.root().unwrap(), Namespace::empty());
    }
    
    processed_symbols
}

fn process_tree<R: gimli::Reader<Offset = usize>>(processed_symbols: &mut ProcessedSymbols, dwarf: &Dwarf<EndianReader<RunTimeEndian, Rc<[u8]>>>, unit: &Unit<EndianReader<RunTimeEndian, Rc<[u8]>>>, node: EntriesTreeNode<R>, namespace: Namespace) {
    let mut children = node.children();
    while let Some(child) = children.next().unwrap() {
        let tag = child.entry().tag();
        let line_program_header = unit.line_program.as_ref().unwrap().header();

        let name: Option<String> = child.entry().attr(gimli::DW_AT_name).unwrap().and_then(|name| match name.value() {
            AttributeValue::DebugStrRef(s) => Some(dwarf.string(s).unwrap().to_string().unwrap().to_string()),
            _ => None,
        });

        let file_entry = child.entry().attr(gimli::DW_AT_decl_file).unwrap().and_then(|v| match v.value() {
            AttributeValue::FileIndex(index) => Some(index),
            _ => None,
        }).map(|v| line_program_header.file(v).unwrap());

        let directory_name = file_entry.and_then(|file| file.directory(&line_program_header)).and_then(|v| match v {
            AttributeValue::String(s) => Some(String::from_utf8_lossy(s.bytes()).to_string()),
            _ => None,
        });

        let file_name = file_entry.and_then(|file| match file.path_name() {
            AttributeValue::String(s) => Some(String::from_utf8_lossy(s.bytes()).to_string()),
            _ => None,
        });

        if tag == gimli::DW_TAG_subprogram && name.is_some() {
            let line = child.entry().attr(gimli::DW_AT_decl_line).unwrap().and_then(|line| match line.value() {
                AttributeValue::Udata(d) => Some(d),
                _ => None,
            });

            processed_symbols.add_function(Function::new(namespace.clone(), name.unwrap(), directory_name, file_name, line));
        } else if tag == gimli::DW_TAG_namespace && name.is_some() {
            let new_namespace = namespace.clone().child_with_name(name.unwrap());
            process_tree(processed_symbols, dwarf, unit, child, new_namespace);
        } else if tag == gimli::DW_TAG_structure_type && name.is_some() {
            let new_namespace = namespace.clone().child_with_name(name.unwrap());
            process_tree(processed_symbols, dwarf, unit, child, new_namespace);
        } else {
            process_tree(processed_symbols, dwarf, unit, child, namespace.clone());
        }
    }
}

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING").unwrap_or("redis://127.0.0.1:6379/".to_owned())
}

pub fn kafka_broker_host() -> String {
    env::var("BROKER_HOST").unwrap_or("127.0.0.1:9092".to_owned())
}
