FROM rust:1.60-bullseye

RUN rustup target add wasm32-unknown-unknown

RUN cargo install --locked trunk