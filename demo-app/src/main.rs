use ::{
    std::{fs, env, io::Read, collections::HashMap, thread, time::Duration},
    actix_web::{HttpServer, Responder, get, post, App, web::{self, PayloadConfig}, HttpResponse},
    env_logger::Env,
    log::{info, error},
    sonogram::{ColourGradient, ColourTheme, FrequencyScale, SpecOptionsBuilder},
    hound,
    rand::Rng,
    continuous_profiler_agent::ContinuousProfiler,
};

const COMMAND_SERVER: &str = "server";
const COMMAND_LOAD_GENERATOR: &str = "load::generate";
const MAX_FILE_SIZE: usize = 100 * 1024 * 1024;

#[actix_web::main]
#[inline(never)]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    
    let command = env::args().collect::<Vec<String>>()
        .get(1)
        .cloned()
        .unwrap_or(COMMAND_SERVER.to_owned());

    let port = env::var("PORT").map(|v| v.parse().unwrap()).unwrap_or(8080);

    match command.as_str() {
        COMMAND_SERVER => {
            info!("starting demo app server version {}", env::var("SERVICE_VERSION").unwrap_or("unknown".to_owned()));

            ContinuousProfiler::new(
                env::var("CONTINUOUS_PROFILER_ENDPOINT").unwrap_or("profiling-coordinator.default.svc.cluster.local:7878".to_owned()),
                "demo-app".to_owned()
            )
                .with_tags({
                    let mut tags = HashMap::new();
                        tags.insert("env".to_owned(), env::var("ENV").unwrap_or("unknown".to_owned()));
                        tags
                    })
                .with_access_token("replace-later-with-real-token".to_owned())
                .with_healthcheck_endpoint(format!("http://localhost:{}/", port))
                .run();

            HttpServer::new(|| App::new()
                .app_data(PayloadConfig::default().limit(MAX_FILE_SIZE))
                .service(index)
                .service(sound2sonogram)
            )
                .bind(("0.0.0.0", port))?
                .run()
                .await
        },
        COMMAND_LOAD_GENERATOR => {
            info!("starting load generator for demo app");
            generate_load().await;
            Ok(())
        },
        other => {
            error!("Unexpected command: {}", other);
            Ok(())
        }
    }
}

#[get("/")]
async fn index() -> impl Responder {
    "ok"
}

#[post("/sound2sonogram")]
#[inline(never)]
async fn sound2sonogram(body: web::Bytes) -> impl Responder {
    info!("converting incoming sound to sonogram");
    let mut reader = hound::WavReader::new(&*body).unwrap();
    let samples: Vec<i16> = reader.samples().into_iter()
        .map(|v| v.unwrap())
        .collect();

    let mut spectrograph = SpecOptionsBuilder::new(2048)
        .load_data_from_memory(samples, reader.spec().sample_rate)
        .build()
        .unwrap();

    let mut spectrogram = spectrograph.compute();

    let mut tmpfile = tempfile::NamedTempFile::new().unwrap();

    let mut gradient = ColourGradient::create(ColourTheme::Default);
    spectrogram.to_png(
        &tmpfile.path(),
        FrequencyScale::Log, 
        &mut gradient, 
        512,
        512
    ).unwrap();

    let mut response = Vec::new();
    tmpfile.read_to_end(&mut response).unwrap();

    // do_something_useless();

    HttpResponse::Ok()
        .append_header(("Content-Type", "image/png"))
        .body(response)
}

async fn generate_load() {
    let client = reqwest::Client::new();
    let endpoint = format!("http://{}", env::var("TARGET_APP_HOST").unwrap_or("127.0.0.1:8080".to_owned()));

    loop {
        info!("making request to sound2sonogram endpoint");
        let res = client.post(format!("{}/sound2sonogram", endpoint))
            .body(fs::read("./demo.wav").unwrap())
            .send()
            .await;
        let res = match res {
            Ok(v) => v,
            Err(err) => {
                error!("request to sound2sonogram failed: {:?}", err);
                thread::sleep(Duration::from_secs(1));
                continue;
            }
        };
        info!("result is: {}", res.status());    
    }
}

fn do_something_useless() {
    info!("starting useless code");
    let mut sum: i32 = 0;
    for i in 0..10_000_000 {
        sum = sum.wrapping_add(rand::thread_rng().gen_range(0..100) + i);
    }
    info!("useless sum is: {}", sum);
}