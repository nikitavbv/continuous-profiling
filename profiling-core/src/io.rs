use serde::Deserialize;

use {
    std::io::Cursor,
    crate::protocol::{
        CollectedProfile, 
        CollectedProfileWithMetadata, 
        Flamegraph, 
        ServiceSymbols,
    },
};

pub fn compress_profile(profile: &CollectedProfile) -> Vec<u8> {
    zstd::stream::encode_all(Cursor::new(&serde_json::to_vec(&profile).unwrap()), 10).unwrap()
}

pub fn decompress_profile(compressed_profile: Vec<u8>) -> CollectedProfile {
    serde_json::from_slice(&zstd::stream::decode_all(Cursor::new(compressed_profile)).unwrap()).unwrap()
}

pub fn compress_profile_with_metadata(profile: &CollectedProfileWithMetadata) -> Vec<u8> {
    zstd::stream::encode_all(Cursor::new(&serde_json::to_vec(&profile).unwrap()), 10).unwrap()
}

pub fn decompress_profile_with_metadata(compressed_profile: Vec<u8>) -> CollectedProfileWithMetadata {
    serde_json::from_slice(&zstd::stream::decode_all(Cursor::new(compressed_profile)).unwrap()).unwrap()
}

pub fn compress_flamegraph(flamegraph: &Flamegraph) -> Vec<u8> {
    zstd::stream::encode_all(Cursor::new(&serde_json::to_vec(&flamegraph).unwrap()), 10).unwrap()
}

pub fn decompress_flamegraph(compressed_flamegraph: Vec<u8>) -> Flamegraph {
    let uncompressed = zstd::stream::decode_all(Cursor::new(compressed_flamegraph)).unwrap();
    let mut deserializer = serde_json::Deserializer::from_slice(&uncompressed);
    deserializer.disable_recursion_limit();
    let deserializer = serde_stacker::Deserializer::new(&mut deserializer);
    Flamegraph::deserialize(deserializer).unwrap()
}

pub fn compress_symbols(symbols: &ServiceSymbols) -> Vec<u8> {
    zstd::stream::encode_all(Cursor::new(&serde_json::to_vec(&symbols).unwrap()), 10).unwrap()
}

pub fn decompress_symbols(compressed_symbols: Vec<u8>) -> ServiceSymbols {
    serde_json::from_slice(&zstd::stream::decode_all(Cursor::new(compressed_symbols)).unwrap()).unwrap()
}