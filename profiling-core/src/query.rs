use {
    std::collections::HashMap,
    crate::{protocol::Flamegraph, top_functions::{TopFunctions, TopFunction}}
};

pub struct ComputeTopFunctionsOperator {
}

impl ComputeTopFunctionsOperator {
    pub fn apply(input: &Flamegraph) -> TopFunctions {
        let mut top_functions: HashMap<String, TopFunction> = HashMap::new();
        let mut nodes_to_visit = vec![input.root.clone()];

        while let Some(node) = nodes_to_visit.pop() {
            let node_name = node.name;
            let node_total_frames = node.count;
            let mut node_children_count = 0;

            for (_, child) in node.children.into_iter() {
                node_children_count += child.count;
                nodes_to_visit.push(child);
            }

            if !top_functions.contains_key(&node_name) {
                top_functions.insert(node_name.clone(), TopFunction::new(node_name.clone()));
            }

            top_functions.get_mut(&node_name).unwrap().increment_frames(node_total_frames, node_children_count);
        }

        TopFunctions::empty()
    }
}