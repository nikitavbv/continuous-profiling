use {
    std::{collections::HashSet, time::SystemTime, env},
    redis::Commands,
    crate::{
        io::{
            compress_flamegraph, 
            decompress_flamegraph, 
            compress_profile, 
            decompress_profile,
            compress_symbols,
            decompress_symbols,
        },
        protocol::{
            REDIS_KEY_PREFIX,
            Flamegraph,
            CollectedProfile,
            ServiceActivity,
            RecentVersions,
            ServiceSymbols,
        },
    }
    
};
pub struct ProfilingState {
    redis_client: redis::Client,
    redis_connection: redis::Connection,
}

impl ProfilingState {
    pub fn new() -> Self {
        let redis_client = redis::Client::open(redis_connection_string()).unwrap();
        let redis_connection = redis_client.get_connection().unwrap();  
        
        Self {
            redis_client,
            redis_connection,
        }
    }

    pub fn create_service_if_not_exists(&mut self, service_name: &str) {
        let key = self.key("services");
        let mut existing_services: HashSet<String> = self.redis_connection.get::<&str, Option<String>>(&key).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(HashSet::new());

        if existing_services.contains(service_name) {
            return;
        }

        existing_services.insert(service_name.to_owned());
        self.redis_connection.set::<&str, String, ()>(&key, serde_json::to_string(&existing_services).unwrap()).unwrap();
    }

    pub fn create_service_version_if_not_exists(&mut self, service_name: &str, service_version: &str) {
        let key = self.key(&format!("services::{}::versions", service_name));
        let mut existing_versions: HashSet<String> = self.redis_connection.get::<&str, Option<String>>(&key).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(HashSet::new());

        if existing_versions.contains(service_version) {
            return;
        }

        existing_versions.insert(service_version.to_owned());
        self.redis_connection.set::<&str, String, ()>(&key, serde_json::to_string(&existing_versions).unwrap()).unwrap();
    }

    pub fn update_recent_versions(&mut self, service_name: &str, service_version: &str, timestamp: SystemTime, total_frames: u32) {
        let key = self.key(&format!("services::{}::recent_versions", service_name));
        let recent_versions: RecentVersions = self.redis_connection.get::<&str, Option<String>>(&key).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(RecentVersions::new());
        let recent_versions = recent_versions.update_with(service_version, timestamp, total_frames);
        self.redis_connection.set::<&str, String, ()>(&key, serde_json::to_string(&recent_versions).unwrap()).unwrap();
    }

    pub fn update_recent_activity(&mut self, service_name: &str, timestamp: SystemTime, total_frames: u32) {
        let key = self.key(&format!("services::{}::recent_activity", service_name));
        let recent_activity: ServiceActivity = self.redis_connection.get::<&str, Option<String>>(&key).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(ServiceActivity::new());
        let recent_activity = recent_activity.update_with(timestamp, total_frames);
        self.redis_connection.set::<&str, String, ()>(&key, serde_json::to_string(&recent_activity).unwrap()).unwrap();
    }

    pub fn save_latest_profile_for_service(&mut self, service_name: &str, profile: &CollectedProfile) {
        let key = self.key(&format!("services::{}::latest_profile", service_name));
        let compressed_profile = compress_profile(profile);
        self.redis_connection.set::<String, Vec<u8>, ()>(key, compressed_profile).unwrap();
    }

    pub fn save_flamegraph(&mut self, service_name: &str, service_version: &str, new_flamegraph: &Flamegraph) {
        let key = self.key(&format!("services::{}::versions::{}::flamegraph", service_name, service_version));
        let compressed_flamegraph = compress_flamegraph(&new_flamegraph);
        self.redis_connection.set::<String, Vec<u8>, ()>(key, compressed_flamegraph).unwrap();
    }

    pub fn services(&mut self) -> Vec<String> {
        self.redis_connection.get::<String, Option<String>>(self.key("services")).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(Vec::new())
    }

    pub fn service_versions(&mut self, service_name: &str) -> Vec<String> {
        self.redis_connection.get::<String, Option<String>>(self.key(&format!("services::{}::versions", service_name))).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(Vec::new())
    }

    pub fn service_recent_versions(&mut self, service_name: &str) -> RecentVersions {
        self.redis_connection.get::<String, Option<String>>(self.key(&format!("services::{}::recent_versions", service_name))).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(RecentVersions::new())
    }

    pub fn service_recent_activity(&mut self, service_name: &str) -> ServiceActivity {
        self.redis_connection.get::<String, Option<String>>(self.key(&format!("services::{}::recent_activity", service_name))).unwrap()
            .map(|v| serde_json::from_str(&v).unwrap())
            .unwrap_or(ServiceActivity::new())
    }

    pub fn latest_profile_for_service(&mut self, service: &str) -> Option<CollectedProfile> {
        self.redis_connection.get::<String, Option<Vec<u8>>>(self.key(&format!("services::{}::latest_profile", service))).unwrap()
            .map(|v| decompress_profile(v))
    }

    pub fn flamegraph_for_service_version(&mut self, service: &str, version: &str) -> Option<Flamegraph> {
        self.redis_connection.get::<String, Option<Vec<u8>>>(self.key(&format!("services::{}::versions::{}::flamegraph", service, version))).unwrap()
            .map(|v| decompress_flamegraph(v))
    }

    pub fn update_service_symbols(&mut self, service_name: &str, service_version: &str, symbols: &ServiceSymbols) {
        let key = self.key(&format!("services::{}::versions::{}::symbols", service_name, service_version));
        let compressed_symbols = compress_symbols(symbols);
        self.redis_connection.set::<String, Vec<u8>, ()>(key, compressed_symbols).unwrap();
    }

    pub fn symbols_for_service_version(&mut self, service: &str, version: &str) -> Option<ServiceSymbols> {
        self.redis_connection.get::<String, Option<Vec<u8>>>(self.key(&format!("services::{}::versions::{}::symbols", service, version))).unwrap()
            .map(|v| decompress_symbols(v))
    }

    fn key(&self, key: &str) -> String {
        format!("{}{}", REDIS_KEY_PREFIX, key)
    }
}

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING").unwrap_or("redis://127.0.0.1:6379/".to_owned())
}