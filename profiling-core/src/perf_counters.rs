use {
    std::time::Duration,
    serde::{Serialize, Deserialize},
};

#[derive(Debug, Serialize, Deserialize)]
pub enum PerfCounterType {
    CPUCycles,
    Instructions,
    CacheReferences,
    CacheMisses,
    BranchInstructions,
    BranchMisses,
    StalledCyclesFrontend,
    StalledCyclesBackend,
    PageFaults,
    PageFaultsMinor,
    PageFaultsMajor,
    ContextSwitches,
    CpuMigrations,
    EmulationFaults,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PerfCounterStats {
    counter_type: PerfCounterType,
    stats: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PerfCounterData {
    counters: Vec<PerfCounterStats>,
    collection_interval: Duration,
}

impl PerfCounterType {
    pub fn description(&self) -> String {
        match self {
            Self::CPUCycles => "Total cpu cycles",
            Self::Instructions => "Count of retired instructions, i.e. instructions that are actually executed and completed. Does not include instructions that were executed as the result of misprediction.",
            
            // see https://stackoverflow.com/questions/55035313/how-does-linux-perf-calculate-the-cache-references-and-cache-misses-events
            // if cacheable memory access missed in the L1 and L2, then it will be counted in cache references
            // if it hits L1, then it will not be counted
            Self::CacheReferences => "Count of cache references. This includes both hits and misses. Usually indicates Last Level Cache accesses, but may vary depending on the CPU.",
            Self::CacheMisses => "Count of cache misses. Usually indicatges Last Level Cache misses, but may vary depending on the CPU.",
            
            Self::BranchInstructions => "Count of retired branch instructions. This means that this counter does not include instructions that were executed as the result of misprediction.",
            Self::BranchMisses => "Count of branch misses.",

            // More details here: https://stackoverflow.com/questions/22165299/what-are-stalled-cycles-frontend-and-stalled-cycles-backend-in-perf-stat-resul
            Self::StalledCyclesFrontend => "Cycles wasted because you have misses in the instruction cache or have complex instructions, or there was a branch prediction miss.",
            Self::StalledCyclesBackend => "Cycles wasted because the CPU has to wait for resources (usually memory) or to finish long latency instructions",
        
            Self::PageFaults => "Event that happens when page is requested but is not available in physical memory yet",
            Self::PageFaultsMinor => "Minor page faults. These did not require disk I/O to handle",
            Self::PageFaultsMajor => "Major page faults. These required disk I/O to handle",
            Self::ContextSwitches => "Context switches",
            Self::CpuMigrations => "Number of times the process has been migrated to a new CPU",
            Self::EmulationFaults => "Number of times unimplemented instructions were emulated"
        }.to_owned()
    }
}

impl PerfCounterStats {
    pub fn new(counter_type: PerfCounterType, stats: u64) -> Self {
        Self {
            counter_type,
            stats,
        }
    }
}

impl PerfCounterData {
    pub fn new(counters: Vec<PerfCounterStats>, collection_interval: Duration) -> Self {
        Self {
            counters,
            collection_interval,
        }
    }

    pub fn empty() -> Self {
        Self::new(Vec::new(), Duration::from_secs(0))
    }
}