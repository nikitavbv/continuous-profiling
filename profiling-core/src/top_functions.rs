use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct TopFunctions {
    functions: Vec<TopFunction>,
}

#[derive(Serialize, Deserialize)]
pub struct TopFunction {
    name: String,
    total_frames: u32,
    total_children_frames: u32,
}

impl TopFunctions {
    pub fn empty() -> Self {
        Self {
            functions: Vec::new(),
        }
    }
}

impl TopFunction {
    pub fn new(name: String) -> Self {
        Self {
            name,
            total_frames: 0,
            total_children_frames: 0,
        }
    }

    pub fn increment_frames(&mut self, total_frames: u32, total_children_frames: u32) {
        self.total_frames += total_frames;
        self.total_children_frames += total_children_frames;
    }
}