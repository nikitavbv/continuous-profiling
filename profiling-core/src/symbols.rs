use {
    std::collections::HashMap,
    regex::Regex,
    lazy_static::lazy_static,
    crate::protocol::{
        Flamegraph,
        FlamegraphNode,
        ServiceSymbols, 
        FunctionOrigin, 
        ServiceBinaryFunction,
        ServiceBinaryNamespace,
    },
};

impl Flamegraph {
    pub fn enrich_with_symbols(&mut self, symbols: &ServiceSymbols) {
        let functions = symbols.functions();
        let mut origin_by_function: HashMap<String, FunctionOrigin> = HashMap::new();

        for function in functions {
            let name = normalize_function_name(function.name());
            let namespace_name = function.namespace().map(|v| v.full_name());
            let full_name = if let Some(namespace_name) = namespace_name {
                format!("{}::{}", namespace_name, name)
            } else {
                name
            };

            if origin_by_function.contains_key(&full_name) {
                continue;
            }

            origin_by_function.insert(full_name, detect_function_origin(function));
        }

        self.root.enrich_with_origin_by_function(&origin_by_function);
    }
}

impl FlamegraphNode {
    pub fn enrich_with_origin_by_function(&mut self, origin_by_function: &HashMap<String, FunctionOrigin>) {
        if let Some(origin) = get_exact_or_variant(origin_by_function, &self.name) {
            self.function_origin = Some(origin.clone());
        }
        
        for (_name, child) in &mut self.children {
            child.enrich_with_origin_by_function(origin_by_function);
        }
    }
}

fn normalize_function_name(name: &str) -> String {
    lazy_static! {
        static ref CLOSURE_RE: Regex = Regex::new("\\{closure#\\d+\\}").unwrap();
    }
    let name = function_name_without_generic(name);
    CLOSURE_RE.replace_all(&name, "{{closure}}").to_string()
}

fn function_name_without_generic(name: &str) -> String {
    let mut result = String::new();
    let mut generic_level = 0;

    for c in name.chars() {
        if c == '<' {
            generic_level += 1;
        } else if c == '>' {
            generic_level -= 1;
        } else if generic_level == 0 {
            result.push(c);
        }
    }

    result
}

fn get_exact_or_variant<'a>(origin_by_function: &'a HashMap<String, FunctionOrigin>, key: &str) -> Option<&'a FunctionOrigin> {
    let key = normalize_function_name(key);
    
    if let Some(origin) = origin_by_function.get(&key) {
        Some(origin)
    } else {
        for (existing_key, value) in origin_by_function.iter() {
            if are_variants_of_same_function(&key, existing_key) {
                return Some(value);
            }
        }
        
        None
    }
}

fn are_variants_of_same_function(first: &str, second: &str) -> bool {
    lazy_static! {
        static ref IMPL_RE: Regex = Regex::new("\\{impl#\\d+\\}").unwrap();
    }

    let first_parts: Vec<String> = first.split("::").map(|v| v.to_owned()).collect();
    let second_parts: Vec<String> = second.split("::").map(|v| v.to_owned()).collect();

    let mut first_index = 0;
    let mut second_index = 0;

    loop {
        if first_index >= first_parts.len() || second_index >= second_parts.len() {
            break;
        }

        let first_part = &first_parts[first_index];
        let second_part = &second_parts[second_index];

        if IMPL_RE.is_match(first_part) || IMPL_RE.is_match(second_part) {
            // ignore in such case
        } else if first_part != second_part {
            return false;
        }

        first_index += 1;
        second_index += 1;
    }

    first_index == first_parts.len() && second_index == second_parts.len()
}

fn detect_function_origin(function: &ServiceBinaryFunction) -> FunctionOrigin {
    if let Some(directory_name) = function.directory_name() {
        if directory_name.starts_with("library/") {
            return FunctionOrigin::StandardLibrary;
        }

        let directory_name_parts = directory_name.split('/').collect::<Vec<_>>();
        if directory_name_parts.get(1) == Some(&"rustc") && directory_name_parts.get(3) == Some(&"library") && directory_name_parts.get(4) == Some(&"std") {
            return FunctionOrigin::StandardLibrary;
        }

        for i in 0..directory_name_parts.len() {
            if directory_name_parts[i] == "cargo" && directory_name_parts.get(i + 1) == Some(&"registry") {
                return FunctionOrigin::ExternalDependency;
            }
        }
    }

    if let Some(file_name) = function.file_name() {
        if file_name.ends_with(".c") || file_name.ends_with(".h") {
            return FunctionOrigin::ExternalDependency;
        }
    }

    FunctionOrigin::Application
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn function_name_without_generic_simple() {
        assert_eq!(
            normalize_function_name("catch_unwind<std::rt::lang_start_internal::{closure#2}, isize>"), 
            "catch_unwind".to_owned()
        );
    }

    #[test]
    fn normalize_function_name_closure() {
        assert_eq!(
            normalize_function_name("{closure#3}"),
            "{{closure}}".to_owned()
        )
    }

    #[test]
    fn normalize_function_name_generic_serde() {
        assert_eq!(
            normalize_function_name("serialize_entry<serde_json::ser::Compound<&mut serde_json::value::{impl#1}::fmt::WriterFormatter, serde_json::ser::PrettyFormatter>, alloc::string::String, serde_json::value::Value>"),
            "serialize_entry".to_owned()
        )
    }

    #[test]
    fn are_variants_of_same_function_thread_impl() {
        assert!(are_variants_of_same_function(
            "std::sys::unix::thread::Thread::new::thread_start",
            "std::sys::unix::thread::{impl#2}::new::thread_start",
        ));
    }
    
    #[test]
    fn function_origin_for_standard_std() {
        assert_eq!(
            detect_function_origin(&ServiceBinaryFunction {
                namespace: ServiceBinaryNamespace::parse("std::panic"),
                name: "catch_unwind<std::rt::lang_start_internal::{closure#2}, isize>".to_owned(),
                directory_name: Some("/rustc/db9d1b20bba1968c1ec1fc49616d4742c1725b4b/library/std/src".to_owned()),
                file_name: Some("panic.rs".to_owned()),
                line: Some(132),
            }),
            FunctionOrigin::StandardLibrary,
        )
    }

    #[test]
    fn function_origin_for_external_dependency() {
        assert_eq!(
            detect_function_origin(&ServiceBinaryFunction {
                namespace: ServiceBinaryNamespace::parse("tokio::runtime::thread_pool::worker"),
                name: "inject".to_owned(),
                directory_name: Some("/usr/local/cargo/registry/src/github.com-1ecc6299db9ec823/tokio-1.19.0/src/runtime/thread_pool".to_owned()),
                file_name: Some("worker.rs".to_owned()),
                line: Some(706),
            }),
            FunctionOrigin::ExternalDependency,
        )
    }

    #[test]
    fn function_origin_for_application() {
        assert_eq!(
            detect_function_origin(&ServiceBinaryFunction {
                namespace: ServiceBinaryNamespace::parse("symbols_processing_job"),
                name: "process_tree<gimli::read::endian_reader::EndianReader<gimli::endianity::RunTimeEndian, alloc::rc::Rc<[u8]>>>".to_owned(),
                directory_name: Some("src".to_owned()),
                file_name: Some("main.rs".to_owned()),
                line: Some(236),
            }),
            FunctionOrigin::Application,
        )
    }

    #[test]
    fn function_origin_for_zstd() {
        assert_eq!(
            detect_function_origin(&ServiceBinaryFunction {
                namespace: None,
                name: "ZSTD_compressedLiterals".to_owned(),
                directory_name: Some("zstd/lib/compress".to_owned()),
                file_name: Some("zstd_opt.c".to_owned()),
                line: Some(66),
            }),
            FunctionOrigin::ExternalDependency,
        )
    }
}