use crate::perf_counters::PerfCounterData;

use {
    std::{time::{Duration, SystemTime}, collections::HashMap},
    serde::{Serialize, Deserialize},
};

pub const REDIS_KEY_PREFIX: &str = "continuous-profiling::";
pub const KAFKA_TOPIC_SERVICE_BINARIES: &str = "service-binaries";
pub const KAFKA_TOPIC_COLLECTED_PERF_COUNTERS: &str = "collected-perf-counters";
pub const KAFKA_AGGREGATIONS_CALCULATION_JOB_CONSUMER_GROUP: &str = "aggregations-calculation-job";
pub const KAFKA_SYMBOLS_PROCESSING_JOB_CONSUMER_GROUP: &str = "symbols-processing-job";
pub const KAFKA_PERF_COUNTERS_PROCESSING_JOB_CONSUMER_GROUP: &str = "perf-counters-processing-job";

pub const VERSION_RECENT_THRESHOLD: Duration = Duration::from_secs(60 * 60 * 24);
pub const ACTIVITY_BUCKET_SIZE: Duration = Duration::from_secs(60 * 20);
pub const ACTIVITY_RECENT_THRESHOLD: Duration = Duration::from_secs(60 * 60 * 24);

// agent to coordinator
#[derive(Serialize, Deserialize, Debug)]
pub struct ProfilingSessionInit {
    pub access_token: Option<String>,
    pub agent_id: String,
    pub service_name: String,
    pub service_version: String,
    pub tags: HashMap<String, String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ProfilingSessionAgentToCoordinatorMessage {
    RequestProfilingTask,
    SubmitCollectedProfileCompressed(Vec<u8>),
    SubmitCollectedPerfCounters(PerfCounterData),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ProfilingSessionCoordinatorToAgentMessage {
    DoNothing,
    RunProfiler(ProfilerTask),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProfilerTask {
    pub run_for: Duration,
    pub frequency: u32,
}

impl Default for ProfilerTask {
    fn default() -> Self {
        Self {
            run_for: Duration::from_secs(10),
            frequency: 1000,
        }
    }
}

// collected profile
#[derive(Serialize, Deserialize, Debug)]
pub struct CollectedProfile {
    pub entries: Vec<CollectedProfileEntry>,
    pub params: ProfilingParams,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CollectedProfileWithMetadata {
    pub version: u32,
    pub agent_id: String,
    pub service_name: String,
    pub service_version: String,
    pub tags: HashMap<String, String>,
    pub collected_profile: CollectedProfile,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AgentMetadata {
    pub version: u32,
    pub agent_id: String,
    pub service_name: String,
    pub service_version: String,
    pub tags: HashMap<String, String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CollectedProfileEntry {
    pub frames: CollectedProfileFrames,
    pub count: u32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CollectedProfileFrames {
    pub symbols: Vec<Vec<CollectedProfileSymbol>>,
    pub thread_name: String,
    pub thread_id: u64,   
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CollectedProfileSymbol {
    pub name: String,
    pub filename: Option<String>,
    pub line_number: Option<u32>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProfilingParams {
    pub started_at: SystemTime,
    pub profiling_duration: Duration,
    pub frequency: u32,
}

// redis structs
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct RecentVersions {
    pub versions: Vec<RecentVersion>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct RecentVersion {
    pub timestamp: SystemTime,
    pub version: String,
    pub total_frames: u32,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ServiceActivity {
    pub buckets: Vec<ServiceActivityBucket>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ServiceActivityBucket {
    pub timestamp: SystemTime,
    pub total_frames: u32,
}

#[derive(Serialize, Deserialize)]
pub struct ServiceSymbols {
    pub functions: Vec<ServiceBinaryFunction>,
}

#[derive(Serialize, Deserialize)]
pub struct ServiceBinaryNamespace {
    pub name: String,
    pub parent: Option<Box<ServiceBinaryNamespace>>,
}

#[derive(Serialize, Deserialize)]
pub struct ServiceBinaryFunction {
    pub namespace: Option<ServiceBinaryNamespace>,
    pub name: String,
    pub directory_name: Option<String>,
    pub file_name: Option<String>,
    pub line: Option<u64>,
}

// flamegraph
#[derive(Serialize, Deserialize, Debug)]
pub struct Flamegraph {
    pub root: FlamegraphNode,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct FlamegraphNode {
    pub name: String,
    pub children: HashMap<String, FlamegraphNode>,
    pub count: u32,
    pub function_origin: Option<FunctionOrigin>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum FunctionOrigin {
    StandardLibrary,
    ExternalDependency,
    Application,
}

// webui api
#[derive(Serialize, Deserialize, Debug)]
pub struct ListServicesResponse {
    pub services: Vec<BasicServiceDetails>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct BasicServiceDetails {
    pub name: String,
    pub recent_versions: RecentVersions,
    pub activity: ServiceActivity,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ListVersionsResponse {
    pub all_versions: Vec<String>,
    pub recent_versions: RecentVersions,
}

// kafka events
#[derive(Serialize, Deserialize)]
pub struct ServiceBinaryUpdateEvent {
    pub service: String,
    pub version: String,
}

// impls
impl CollectedProfileEntry {
    fn top_symbol(&self) -> Option<&CollectedProfileSymbol> {
        self.frames.top_symbol()
    }

    fn without_top_symbol(&self) -> CollectedProfileEntry {
        CollectedProfileEntry {
            frames: self.frames.without_top_symbol(),
            count: self.count,
        }
    }
}

impl CollectedProfileFrames {
    fn top_symbol(&self) -> Option<&CollectedProfileSymbol> {
        if self.symbols.is_empty() {
            return None;
        }
        
        Some(self.symbols.get(self.symbols.len() - 1)
            .and_then(|v| v.get(v.len() - 1))
            .expect("expected symbol to be present"))
    }

    fn without_top_symbol(&self) -> CollectedProfileFrames {
        let mut new_symbols = self.symbols.clone();
        if !new_symbols.is_empty() {
            let mut top_symbol = new_symbols.pop().unwrap();
            top_symbol.pop();
            if !top_symbol.is_empty() {
                new_symbols.push(top_symbol);
            }
        }

        CollectedProfileFrames {
            symbols: new_symbols,
            thread_name: self.thread_name.clone(),
            thread_id: self.thread_id,
        }
    }
}

impl Flamegraph {
    pub fn empty() -> Self {
        Self {
            root: FlamegraphNode::empty(),
        }
    }

    pub fn from_collected_profile(profile: &CollectedProfile) -> Self {
        let mut root = FlamegraphNode::empty();
        
        for entry in &profile.entries {
            root.add_profile_entry(&entry);
        }

        Self { 
            root,
        }
    }

    pub fn merge(&self, other: &Self) -> Self {
        Flamegraph {
            root: self.root.merge(&other.root),
        }
    }
}

impl FlamegraphNode {
    pub fn empty() -> Self {
        Self {
            name: "".to_string(),
            children: HashMap::new(),
            count: 0,
            function_origin: None,
        }
    }

    pub fn merge(&self, other: &Self) -> Self {
        let mut merged_children = self.children.clone();
        for (key, other_child) in &other.children {
            if merged_children.contains_key(key) {
                let existing = merged_children.remove(key).unwrap();
                let merged = existing.merge(&other_child);
                merged_children.insert(key.to_owned(), merged);
            } else {
                merged_children.insert(key.to_owned(), other_child.clone());
            }
        }

        Self {
            name: self.name.clone(),
            children: merged_children,
            count: self.count + other.count,
            function_origin: if let Some(node_type) = &other.function_origin {
                Some(node_type.clone())
            } else if let Some(node_type) = &self.function_origin {
                Some(node_type.clone())
            } else {
                None
            },
        }
    }

    pub fn add_profile_entry(&mut self, profile_entry: &CollectedProfileEntry) {
        let count = profile_entry.count;
        self.count += count;

        if let Some(top_symbol) = profile_entry.top_symbol() {
            let key = top_symbol.name.clone();

            if !self.children.contains_key(&key) {
                self.children.insert(key.to_string(), FlamegraphNode {
                    name: top_symbol.name.clone(),
                    children: HashMap::new(),
                    count,
                    function_origin: None, 
                });
            } else {
                self.children.get_mut(&key).unwrap().add_profile_entry(&profile_entry.without_top_symbol());
            }
        }
    }
}

impl RecentVersions {
    pub fn new() -> Self {
        Self {
            versions: Vec::new(),
        }
    }

    pub fn update_with(&self, service_version: &str, timestamp: SystemTime, total_frames: u32) -> Self {
        let mut versions = self.versions.clone();

        let mut found = false;
        for version in &mut versions {
            if version.version == service_version {
                if timestamp > version.timestamp {
                    version.timestamp = timestamp;
                }
                version.total_frames += total_frames;
                found = true;
            }
        }

        if !found {
            versions.push(RecentVersion {
                timestamp,
                version: service_version.to_owned(),
                total_frames,
            });
        }

        let max_timestamp = versions.iter().map(|v| v.timestamp).max().unwrap();
        let recent_versions = versions.into_iter()
            .filter(|v| v.timestamp > max_timestamp - VERSION_RECENT_THRESHOLD)
            .collect();

        Self {
            versions: recent_versions,
        }
    }
}

impl ServiceActivity {
    pub fn new() -> Self {
        Self {
            buckets: Vec::new(),
        }
    }

    pub fn update_with(&self, timestamp: SystemTime, total_frames: u32) -> Self {
        let mut buckets = self.buckets.clone();
    
        let mut found = false;
        for bucket in &mut buckets {
            let time_diff = if bucket.timestamp > timestamp {
                bucket.timestamp.duration_since(timestamp)
            } else {
                timestamp.duration_since(bucket.timestamp)
            }.unwrap();
            if time_diff < ACTIVITY_BUCKET_SIZE {
                bucket.total_frames += total_frames;
                found = true;
            }
        }

        if !found {
            buckets.push(ServiceActivityBucket {
                timestamp,
                total_frames,
            });
        }

        let max_timestamp = buckets.iter().map(|v| v.timestamp).max().unwrap();
        let recent_buckets = buckets.into_iter()
            .filter(|v| v.timestamp > max_timestamp - ACTIVITY_RECENT_THRESHOLD)
            .collect();

        Self { 
            buckets: recent_buckets,
        }
    }
}

impl ServiceSymbols {
    pub fn functions(&self) -> &Vec<ServiceBinaryFunction> {
        &self.functions
    }
}

impl ServiceBinaryFunction {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn namespace(&self) -> Option<&ServiceBinaryNamespace> {
        self.namespace.as_ref()
    }

    pub fn file_name(&self) -> Option<&String> {
        self.file_name.as_ref()
    }

    pub fn directory_name(&self) -> Option<&String> {
        self.directory_name.as_ref()
    }
}

impl ServiceBinaryNamespace {
    pub fn parse(namespace: &str) -> Option<Self> {
        let parts = namespace.split("::");
        let mut result = None;

        for part in parts {
            result = Some(Self {
                name: part.to_owned(),
                parent: result.map(|v| Box::new(v)),
            })
        }

        result
    }

    pub fn full_name(&self) -> String {
        if let Some(parent) = &self.parent {
            format!("{}::{}", parent.full_name(), self.name)
        } else {
            self.name.clone() 
        }
    }
}