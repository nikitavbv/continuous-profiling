#![recursion_limit = "2048"]

use {
    std::{env, sync::{Arc, Mutex}, collections::HashMap},
    log::{info, warn},
    env_logger::Env,
    actix_web::{App, HttpServer, Responder, web, get, HttpResponse},
    serde::Deserialize,
    continuous_profiler_agent::ContinuousProfiler,
    profiling_core::{
        state::ProfilingState,
        protocol::{BasicServiceDetails, ListServicesResponse, ListVersionsResponse}
    },
};

#[derive(Deserialize)]
struct ServiceVersionPath {
    service: String,
    version: String,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    info!("starting continuous profiler query service...");

    let state_store: web::Data<Arc<Mutex<ProfilingState>>> = web::Data::new(Arc::new(Mutex::new(ProfilingState::new())));

    ContinuousProfiler::new(
        env::var("CONTINUOUS_PROFILER_ENDPOINT").unwrap_or("profiling-coordinator.default.svc.cluster.local:7878".to_owned()),
        "query-service".to_owned()
    )
        .with_tags({
            let mut tags = HashMap::new();
            tags.insert("env".to_owned(), env::var("ENV").unwrap_or("unknown".to_owned()));
            tags
        })
        .run();

    HttpServer::new(move || App::new()
        .app_data(state_store.clone())
        .service(index)
        .service(get_services)
        .service(service_versions)
        .service(service_latest_profile)
        .service(service_version_flamegraph)
        .service(service_version_symbols)
    ).bind(("0.0.0.0", 8080))?
        .run()
        .await
}

#[get("/")]
async fn index() -> impl Responder {
    "ok"
}

#[get("/api/v1/services")]
async fn get_services(state_store: web::Data<Arc<Mutex<ProfilingState>>>) -> impl Responder {
    let mut state_store = match state_store.lock() {
        Ok(v) => v,
        Err(err) => {
            warn!("lock posion error occurred");
            err.into_inner()
        }
    };
    let services: Vec<BasicServiceDetails> = state_store.services().into_iter()
        .map(|service_name| BasicServiceDetails {
            recent_versions: state_store.service_recent_versions(&service_name),
            activity: state_store.service_recent_activity(&service_name),
            name: service_name,
        })
        .collect();

    HttpResponse::Ok().json(ListServicesResponse { services })
}

#[get("/api/v1/services/{service}/versions")]
async fn service_versions(service: web::Path<String>, state_store: web::Data<Arc<Mutex<ProfilingState>>>) -> impl Responder {
    let mut state_store = match state_store.lock() {
        Ok(v) => v,
        Err(err) => {
            warn!("lock poision error occured in service_versions");
            err.into_inner()
        }
    };
    let all_versions = state_store.service_versions(&service);
    let recent_versions = state_store.service_recent_versions(&service);

    HttpResponse::Ok()
        .json(ListVersionsResponse {
            all_versions,
            recent_versions,
        })
}

#[get("/api/v1/services/{service}/latest-profile")]
async fn service_latest_profile(state_store: web::Data<Arc<Mutex<ProfilingState>>>, service: web::Path<String>) -> impl Responder {
    HttpResponse::Ok().json(state_store.lock().unwrap().latest_profile_for_service(&service))
}

#[get("/api/v1/services/{service}/versions/{version}/flamegraph")]
async fn service_version_flamegraph(state_store: web::Data<Arc<Mutex<ProfilingState>>>, params: web::Path<ServiceVersionPath>) -> impl Responder {
    HttpResponse::Ok().json(state_store.lock().unwrap().flamegraph_for_service_version(&params.service, &params.version))
}

#[get("/api/v1/services/{service}/versions/{version}/symbols")]
async fn service_version_symbols(state_store: web::Data<Arc<Mutex<ProfilingState>>>, params: web::Path<ServiceVersionPath>) -> impl Responder {
    HttpResponse::Ok().json(state_store.lock().unwrap().symbols_for_service_version(&params.service, &params.version))
}

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING").unwrap_or("redis://127.0.0.1:6379/".to_owned())
}