use {
    env_logger::Env,
    log::info,
    continuous_profiler_agent::perf_counters::collect_perf_counters,
};

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    info!("running ctl: {:?}", collect_perf_counters());
}
