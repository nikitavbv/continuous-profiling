use {
    std::time::Duration,
    log::warn,
    profiling_core::perf_counters::PerfCounterData,
};

#[cfg(target_os = "linux")]
pub fn collect_perf_counters(run_for: &Duration) -> PerfCounterData {
    use {
        std::thread, 
        log::{info, error}, 
        perfcnt::{
            PerfCounter, 
            AbstractPerfCounter, 
            linux::{PerfCounterBuilderLinux, SoftwareEventType, HardwareEventType},
        },
        profiling_core::perf_counters::{PerfCounterStats, PerfCounterType},
    };

    info!("starting performance counters collection...");
    let software_events: Vec<(PerfCounter, PerfCounterType)> = vec![
        (SoftwareEventType::PageFaults, PerfCounterType::PageFaults),
        (SoftwareEventType::ContextSwitches, PerfCounterType::ContextSwitches),
        (SoftwareEventType::CpuMigrations, PerfCounterType::CpuMigrations),
        (SoftwareEventType::PageFaultsMin, PerfCounterType::PageFaultsMinor),
        (SoftwareEventType::PageFaultsMaj, PerfCounterType::PageFaultsMajor),
        (SoftwareEventType::EmulationFaults, PerfCounterType::EmulationFaults),
    ].into_iter()
        .map(|event| (match PerfCounterBuilderLinux::from_software_event(event.0)
            .for_pid(std::process::id() as i32)
            .finish() {
                Ok(v) => Some(v),
                Err(err) => {
                    error!("failed to create software performance counter {:?}: {:?}", event.1, err);
                    None
                }
            },
            event.1
        ))
        .filter(|v| v.0.is_some())
        .map(|v| (v.0.unwrap(), v.1))
        .collect();

    let hardware_events: Vec<(PerfCounter, PerfCounterType)> = vec![
        (HardwareEventType::CPUCycles, PerfCounterType::CPUCycles),
        (HardwareEventType::Instructions, PerfCounterType::Instructions),
        (HardwareEventType::CacheReferences, PerfCounterType::CacheReferences),
        (HardwareEventType::CacheMisses, PerfCounterType::CacheMisses),
        (HardwareEventType::BranchInstructions, PerfCounterType::BranchInstructions),
        (HardwareEventType::BranchMisses, PerfCounterType::BranchMisses),
        (HardwareEventType::StalledCyclesFrontend, PerfCounterType::StalledCyclesFrontend),
        (HardwareEventType::StalledCyclesBackend, PerfCounterType::StalledCyclesBackend),
    ].into_iter()
        .map(|event| (match PerfCounterBuilderLinux::from_hardware_event(event.0)
            .finish() {
                Ok(v) => Some(v),
                Err(err) => {
                    error!("failed to create hardware performance counter {:?}: {:?}", event.1, err);
                    None
                }
            },
            event.1
        ))
        .filter(|v| v.0.is_some())
        .map(|v| (v.0.unwrap(), v.1))
        .collect();

    let mut events: Vec<(PerfCounter, PerfCounterType)> = software_events.into_iter().chain(hardware_events.into_iter()).collect();

    for (counter, counter_type) in events.iter_mut() {
        counter.start().unwrap();
    }

    thread::sleep(run_for.clone());

    for (counter, counter_type) in events.iter_mut() {
        counter.stop().unwrap();   
    }

    let mut results = Vec::new();

    for (mut counter, counter_type) in events {
        results.push(PerfCounterStats::new(counter_type, counter.read().unwrap()));
    }

    PerfCounterData::new(results, run_for.clone())
}

#[cfg(target_os = "macos")]
pub fn collect_perf_counters(_run_for: &Duration) -> PerfCounterData {
    warn!("perf counters are only available on Linux");
    PerfCounterData::empty()
}