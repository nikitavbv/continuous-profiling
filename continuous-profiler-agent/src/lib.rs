use {
    std::{
        env,
        thread, 
        time::Duration, 
        collections::HashMap, 
        net::ToSocketAddrs, 
        io::Cursor, 
        sync::{atomic::{AtomicUsize, Ordering}, Arc},
    },
    log::{info, warn, error},
    rand::{prelude::*, distributions::Alphanumeric},
    thiserror::Error,
    rsocket_rust::{prelude::*, Client},
    rsocket_rust_transport_tcp::TcpClientTransport,
    actix_rt::{Runtime, time::timeout},
    bytes::Bytes,
    pprof::{Report, Frames, Symbol},
    reqwest::StatusCode,
    profiling_core::protocol::{
        ProfilingSessionInit, 
        ProfilingSessionAgentToCoordinatorMessage, 
        ProfilingSessionCoordinatorToAgentMessage,
        CollectedProfile,
        ProfilingParams,
        CollectedProfileEntry,
        CollectedProfileSymbol,
        CollectedProfileFrames,
    },
};

pub mod perf_counters;

const COORDINATOR_RECONNECT_INTERVAL: Duration = Duration::from_secs(5);
const PROFILER_ACTIVATION_INTERVAL: Duration = Duration::from_secs(60);
const KEEPALIVE_TICK_PERIOD: Duration = Duration::from_secs(1);
const KEEPALIVE_TICK_TIMEOUT: Duration = Duration::from_secs(5);
const KEEPALIVE_MISSED_ACKS: u64 = 3;
const COORDINATOR_REQUEST_TIMEOUT: Duration = Duration::from_secs(5);

pub struct ContinuousProfiler {
    coordinator_endpoint: String,
    access_token: Option<String>,
    agent_id: String,
    service_name: String,
    service_version: Option<String>,
    tags: HashMap<String, String>,

    profiling_mode: ProfilingMode,
    active_workload_counter: Arc<AtomicUsize>,

    healthcheck_endpoint: Option<String>,
}

#[derive(Error, Debug)]
enum ContinuousProfilerError {
    #[error("failed to connect to coordinator: {0}")]
    FailedToConnectToCoordinator(String),
    #[error("send message timeout")]
    SendMessageTimeout,
}

#[derive(PartialEq)]
pub enum ProfilingMode {
    Standard,
    WorkloadAware,
}

pub struct ContinuousProfilerControls {
    active_workload_counter: Arc<AtomicUsize>,
}

impl ContinuousProfiler {
    pub fn new(coordinator_endpoint: String, service_name: String) -> Self {
        Self {
            coordinator_endpoint,
            access_token: None,
            agent_id: generate_agent_id(),
            service_name,
            service_version: env::var("SERVICE_VERSION").ok(),
            tags: HashMap::new(),

            profiling_mode: ProfilingMode::Standard,
            active_workload_counter: Arc::new(AtomicUsize::new(0)),
            
            healthcheck_endpoint: None,
        }
    }

    pub fn with_service_version(self, service_version: String) -> Self {
        Self {
            service_version: Some(service_version),
            ..self
        }
    }

    pub fn with_tags(self, tags: HashMap<String, String>) -> Self {
        Self {
            tags,
            ..self
        }
    }

    pub fn with_access_token(self, access_token: String) -> Self {
        Self {
            access_token: Some(access_token),
            ..self
        }
    }

    pub fn with_profiling_mode(self, profiling_mode: ProfilingMode) -> Self {
        Self {
            profiling_mode,
            ..self
        }
    }

    pub fn workload_aware(self) -> Self {
        self.with_profiling_mode(ProfilingMode::WorkloadAware)
    }

    pub fn with_healthcheck_endpoint(self, healthcheck_endpoint: String) -> Self {
        Self {
            healthcheck_endpoint: Some(healthcheck_endpoint),
            ..self
        }
    }

    pub fn run(self) -> ContinuousProfilerControls {
        let controls = ContinuousProfilerControls {
            active_workload_counter: self.active_workload_counter.clone(),
        };

        thread::spawn(move || {
            let runtime = Runtime::new().unwrap();
            
            runtime.block_on(async {
                self.run_profiler_loop().await
            });
        });

        controls
    }

    async fn run_profiler_loop(&self) {
        info!("profiler is started");
    
        let mut client = None;

        loop {
            thread::sleep(PROFILER_ACTIVATION_INTERVAL);
            info!("running profiler iteration, active workload is: {}", self.active_workload_counter.load(Ordering::Relaxed));

            if self.profiling_mode == ProfilingMode::WorkloadAware {
                info!("profiling mode is workload aware, so doing nothing here.");
                continue;
            }

            if client.is_none() {
                info!("connecting to profiling coordinator");
                client = Some(loop {
                    match self.connect_to_coordinator().await {
                        Ok(client) => break client,
                        Err(err) => {
                            error!("failed to connect to coordinator: {}", err);
                            thread::sleep(COORDINATOR_RECONNECT_INTERVAL);
                            continue;
                        }
                    }
                });
            }

            if let Some(healthcheck_endpoint) = &self.healthcheck_endpoint {
                let status = reqwest::get(healthcheck_endpoint).await.unwrap().status();
                if status != StatusCode::OK {
                    warn!("skipping this profiling iteration, because healtcheck status is: {}", status);
                    continue;
                }
            }

            let response = match self.send_message_to_coordinator_with_timeout(
                &client.as_ref().unwrap(), 
                &ProfilingSessionAgentToCoordinatorMessage::RequestProfilingTask
            ).await {
                Ok(v) => v,
                Err(err) => {
                    error!("failed to exchange messages with coordinator: {:?}", err);
                    client = None;
                    continue;
                }
            };
            info!("message from coordinator: {:?}", response);
                        
            let client = client.as_ref().unwrap();

            match response {
                ProfilingSessionCoordinatorToAgentMessage::DoNothing => continue,
                ProfilingSessionCoordinatorToAgentMessage::RunProfiler(profiler_task) => {
                    let prof_thread = pprof::ProfilerGuard::new(profiler_task.frequency as i32).unwrap();
                    thread::sleep(profiler_task.run_for);

                    let report = match prof_thread.report().build() {
                        Ok(v) => v,
                        Err(err) => {
                            error!("failed to build profiling report: {:?}", err);
                            continue;
                        }
                    };

                    let collected_profile = collected_profile_from_pprof_report(&report);
                    let collected_profile_json = serde_json::to_vec(&collected_profile).unwrap();
                    let collected_profile_zstd = zstd::stream::encode_all(Cursor::new(&collected_profile_json), 10).unwrap();

                    info!("done collecting profile, sending to coordinator...");
                    self.send_message_to_coordinator(&client, &ProfilingSessionAgentToCoordinatorMessage::SubmitCollectedProfileCompressed(collected_profile_zstd)).await;

                    info!("collecting perf counters...");
                    let perf_counters = perf_counters::collect_perf_counters(&profiler_task.run_for);
                    self.send_message_to_coordinator(&client, &ProfilingSessionAgentToCoordinatorMessage::SubmitCollectedPerfCounters(perf_counters)).await;

                    info!("profiler iteration finished");
                }
            }
        }
    }

    async fn send_message_to_coordinator_with_timeout(&self, client: &Client, msg: &ProfilingSessionAgentToCoordinatorMessage) -> Result<ProfilingSessionCoordinatorToAgentMessage, ContinuousProfilerError> {
        timeout(COORDINATOR_REQUEST_TIMEOUT, async {
            self.send_message_to_coordinator(client, msg).await
        }).await.map_err(|_| ContinuousProfilerError::SendMessageTimeout)
    }

    async fn send_message_to_coordinator(&self, client: &Client, msg: &ProfilingSessionAgentToCoordinatorMessage) -> ProfilingSessionCoordinatorToAgentMessage {
        let payload = Payload::new(Some(Bytes::from(serde_json::to_vec(&msg).unwrap())), None);
        let response = client.request_response(payload).await.unwrap().unwrap();
        let response: ProfilingSessionCoordinatorToAgentMessage = serde_json::from_slice(&response.data().unwrap().to_vec()).unwrap();
        response
    }

    async fn connect_to_coordinator(&self) -> Result<Client, ContinuousProfilerError>  {
        let endpoint = self.coordinator_endpoint.to_socket_addrs()
            .unwrap()
            .choose(&mut rand::thread_rng())
            .unwrap()
            .to_string();

        info!("connecting to coordinator at {}", endpoint);

        RSocketFactory::connect()
            .transport(TcpClientTransport::from(endpoint))
            .setup(Payload::new(Some(Bytes::from(serde_json::to_vec(&ProfilingSessionInit {
                agent_id: self.agent_id.clone(),
                service_name: self.service_name.clone(),
                service_version: self.service_version.as_ref().cloned().unwrap(),
                tags: self.tags.clone(),
                access_token: self.access_token.clone(),
            }).unwrap())), None))
            .mime_type("application/json", "application/json")
            .keepalive(KEEPALIVE_TICK_PERIOD, KEEPALIVE_TICK_TIMEOUT, KEEPALIVE_MISSED_ACKS)
            .on_close(Box::new(|| info!("connection to profiling coordinator is closed")))
            .start()
            .await
            .map_err(|err| ContinuousProfilerError::FailedToConnectToCoordinator(err.to_string()))
    }
}

impl ContinuousProfilerControls {
    pub fn on_workload_start(&self) {
        self.active_workload_counter.fetch_add(1, Ordering::Relaxed);
    }

    pub fn on_workload_stop(&self) {
        self.active_workload_counter.fetch_sub(1, Ordering::Relaxed);
    }
}

fn collected_profile_from_pprof_report(report: &Report) -> CollectedProfile {
    CollectedProfile {
        params: ProfilingParams {
            started_at: report.timing.start_time.clone(),
            profiling_duration: report.timing.duration.clone(),
            frequency: report.timing.frequency as u32,
        },
        entries: report.data.iter()
            .map(|entry| collected_profile_entry_from_pprof_entry(entry.0, *entry.1))
            .collect()
    }
}

fn collected_profile_entry_from_pprof_entry(entry: &Frames, count: isize) -> CollectedProfileEntry {
    CollectedProfileEntry {
        frames: collected_profile_frames_from_pprof_frames(entry),
        count: count as u32,
    }
}

fn collected_profile_frames_from_pprof_frames(frame: &Frames) -> CollectedProfileFrames {
    CollectedProfileFrames {
        symbols: frame.frames.iter()
            .map(|v| v.iter().map(|vs| collected_profile_symbol_from_pprof_symbol(vs)).collect())
            .collect(),
        thread_name: frame.thread_name.clone(),
        thread_id: frame.thread_id,
    }
}

fn collected_profile_symbol_from_pprof_symbol(symbol: &Symbol) -> CollectedProfileSymbol {
    CollectedProfileSymbol {
        name: symbol.name(),
        filename: match symbol.filename().to_string().as_str() {
            "Unknown" => None,
            other => Some(other.to_owned()),
        },
        line_number: match symbol.lineno() {
            0 => None,
            other => Some(other as u32),
        },
    }
}

fn generate_agent_id() -> String {
    thread_rng().sample_iter(&Alphanumeric).take(11).map(char::from).collect()
}