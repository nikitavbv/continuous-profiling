# Continuous Profiling

## Profiling strategy configuration

The main parameter to tweak here is `profiling_rate`. Profiling rate is a probability that any given instance will participate in profiling iteration.

For example, if you have 10 instances of your application, you can expect that with `profiling_rate` of `0.4` you will have 4 instances on average running continuous profiler. If you only have one instance of your application, than with the same `profiling_rate` you can expect profiler to run once every two profiling iterations.

By changing `profiling_frequency` you can control sampling rate of the profiler (the higher the frequency, the more profiles are collected, but the load on your application is also higher).

Finally, you can change `profiling_duration` to control how long the profiler is running each profiling iteration.

## Local Development

Forward query-service to local machine:
```
kubectl port-forward service/query-service 8081:8080
```

Now serve the frontend locally using backend proxy:
```
trunk serve --proxy-backend=http://localhost:8081/api/
```

## Profiler agent integration

It seems that pprof's `Symbol::addr` does not return any information for functions of the application. I need to research for other options to extract more information about symbols.

To build a binary with all needed symbols included, the following needs to be added to `Cargo.toml` of your service:
```
[profile.dev]
opt-level = 2
```
Please note that it does not affect the binary deployed to production, only the binary you submit to continuous profiler to extract symbols and match them with profiles.

To collect perf counters like context switching and cache misses, you should be running container in privileged context.

## Kafka setup
I used strimzi kafka operator:
https://strimzi.io/quickstarts/

It turned out that there are problems with deploying it to GKE Autopilot ([see discussion here](https://github.com/strimzi/strimzi-kafka-operator/discussions/6111)), so I deployed the whole infra to GKE Standard instead.

## Communication between services

Communication between agent and coordinator is performed using rsocket.

Collected profiles are serialized to json compressed with zstd. This achieves profile size of just ~4KB for profiler running with frequency of 1000 for 10 seconds. If json is replaced with some binary format like msgpack, the resulting side is still comparable.

## Profiling coordinator

Profiling coordinator sends collected profiles to `collected-profiles` kafka topic.

## Calculating aggregations

In order to build framegraph, we need to calculate aggregations, for example by service and version.

Aggregations are calculated by `profiles-processing-job`.

Profiles processing job is a service which is separate from profiling coordinator. That is because you may want to scale it to zero to reset offsets, while keeping all the other profiling infra up.

## Maintenance

### How to login to google container registry from non-GKE kubernetes cluster

```
kubectl create secret docker-registry container-registry-creds \
  --docker-server=eu.gcr.io \
  --docker-username=_json_key \
  --docker-password="$(cat ./infrastructure/.service-account-key.json)" \
  --docker-email=continuous-profiling-deploy@nikitavbv.iam.gserviceaccount.com
```

### How to check kafka consumer offsets

First, exec shell in kafka container:
```
kubectl exec -n continuous-profiling-kafka profiling-kafka-kafka-0 -it /bin/bash
```

Then run this command:
```
./bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --all-groups
```

### How to reset kafka consumer offset (for example to recalculate aggregations)

First of all, you need to bring down all aggregations job instances. For that change `replicas` for profiles processing job and `kubectl apply -f` it.

First, setup port forwarding of profiling coordinator to local machine:
```
kubectl port-forward service/profiling-coordinator 8080
```

Next, perform the following request to clear Redis:
```
POST http://localhost:8080/private/v1/maintenance/recalculate-aggregations
```

You can stop port-forwarding now.

Now exec shell in kafka container:
```
kubectl exec -n continuous-profiling-kafka profiling-kafka-kafka-0 -it /bin/bash
```

Run this command to see current offsets:
```
./bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group aggregations-calculation-job
```

Reset offsets to earliest using this command:
```
./bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --group aggregations-calculation-job --topic collected-profiles --reset-offsets --to-earliest --execute
```

You can double check that offsets has been reset using the previous command.

Finally, bring up all aggregations job instances back.

### How to reset symbols processing job

Similarly:
```
./bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group symbols-processing-job

./bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --group symbols-processing-job --topic service-binaries --reset-offsets --to-earliest --execute
```

## Debug vs release builds

Due to various compiler optimizations and inlining collected profiles for release builds contain less details than debug builds. In fact, some functions can be completely missing from release builds.

We want to collect flamegraphs which provide enough information to guide optimizations. At the same time, we can't run all the services in debug mode only because that will mean reduced performance and, what is more important, inaccurate profiling information.

## Ideas and todos
- Connect with prometheus metrics somehow?
  - see if I can distinguish actix endpoints
    - endpoint performance is endpoint frames / root
  - build vmselect integration to show performance in terms of seconds as well
- Connect with git sources (show flamegraph data next to the code)
  - also add source code (extract it from binary as well)
  - I would like to show a card with some details when a node on flamegraph is selected (right-click)
    - this card will contain info about source code. There will be a button you can click to actually view it.
- Anomaly detection
  - todo: deploy demo app built with debug mode to see if there is any difference
  - Any function from the list started taking percentage more (or less) than usual
- Add search to service list
- Encryption of stored data with kms?
  - just for fun
- Collect more system information
  - GWP collects hardware events, lock contention profiles, kernel events, harware performance monitoring event profiles, kernel event traces and power measurements, CPU cycles, retired instructions, L1 and L2 cache misses, branch misspredictions, heap memory allocations, lock contention time.
  - Collect some information about the hardware.
- perform some benchmarks to measure overhead (test different frequencies)
  - perform this benchmark in a separate crate, so you can run it anytime
- compare stats between service versions
  - first, add table view
    - for table view, we need calculate functions with most self time, i.e. frames - children.frames
- serverless mode
  - added hooks to agent, nothing else is implemented at this moment. Added integration to test service, need to see how it works (should count requests).
  - at first, you set workload_aware_profiling to `true` and notify coordinator that you are planning to use this mode.
  - when workload is added, agent starts one profiling iteration in case it has not been started already and enough time has passed.
  - when workload is removed and workload counter reaches zero we notify profiler to stop profiling and wait until the results are sent.
- integrate via token
  - read tokens from config (but ignore them at this moment)
  - make it possible to disallow connections without tokens
- push metrics to VictoriaMetrics (metrics for configured functions)
- integrate with running tests on ci
- data retention
- latency test
- profile completeness test
- flamegraph search
- make it possible to switch between flamegraph rendering modes
  - see [this](https://www.brendangregg.com/blog/2017-07-30/coloring-flamegraphs-code-type.html)
- aggregations by library
- aggregations by function - finally solve that problem with recursive calls
- frontend: support versions in paths
- make arbitrary requests possible
  - will probably be map-reduce style query execution with storage in Redis?
- profile guided optimizations during compilation - can we do it?
