use profiling_core::protocol::FunctionOrigin;

use {
    yew::{prelude::*, virtual_dom::VNode},
    log::info,
    gloo_net::http::Request,
    web_sys::HtmlInputElement,
    wasm_bindgen_futures::spawn_local,
    wasm_bindgen::JsCast,
    serde::Deserialize,
    profiling_core::protocol::{Flamegraph as FlamegraphData, FlamegraphNode as FlamegraphNodeData},
};

const STANDARD_LIBRARY_COLOR: &str = "#718093";
const EXTERNAL_DEPENDENCY_COLOR: &str = "#487eb0";
const APPLICATION_COLOR: &str = "#8c7ae6";
const UNKNOWN_COLOR: &str = "#2f3640";

#[derive(Properties, Clone, PartialEq)]
pub struct FlamegraphProps {
    pub service: String,
    pub version: String,
}

#[derive(Properties, Clone, PartialEq)]
pub struct FlamegraphNodeProps {
    pub layer: u32,
    pub path: Vec<String>,
    pub selected_path: Vec<String>,
    pub data: FlamegraphNodeData,
    pub parent_frames: Option<u32>,
    pub search_query: String,
    pub onselect: Callback<Vec<String>>,
}

#[derive(Debug)]
enum FlamegraphDataState {
    Loading,
    Loaded {
        flamegraph: FlamegraphData,
        version: String,
    }
}

#[function_component(Flamegraph)]
pub fn flamegraph(props: &FlamegraphProps) -> Html {
    let flamegraph_data = use_state(|| FlamegraphDataState::Loading);
    let selected_path = use_state(|| Vec::<String>::new());
    let search_query = use_state(|| "".to_owned());

    {
        let flamegraph_data_setter = flamegraph_data.setter().clone();
        let service = props.service.clone();
        let version = props.version.clone();

        let needs_loading = match &*flamegraph_data {
            FlamegraphDataState::Loading => true,
            FlamegraphDataState::Loaded { version, .. } => {
                let needs_loading = version != &props.version;

                if needs_loading {
                    flamegraph_data_setter.set(FlamegraphDataState::Loading);
                }

                needs_loading
            }
        };

        use_effect_with_deps(move |version| {
            let version = version.clone();

            spawn_local(async move {
                if needs_loading {
                    let flamegraph_data: Vec<u8> = Request::get(&format!("/api/v1/services/{}/versions/{}/flamegraph", service, version))
                        .send()
                        .await
                        .unwrap()
                        .binary()
                        .await
                        .expect("failed to get flamegraph response");

                    let mut deserializer = serde_json::Deserializer::from_slice(&flamegraph_data);
                    deserializer.disable_recursion_limit();
                    let deserializer = serde_stacker::Deserializer::new(&mut deserializer);
                        
                    let flamegraph_data = FlamegraphData::deserialize(deserializer).unwrap();
                    
                    flamegraph_data_setter.set(FlamegraphDataState::Loaded { flamegraph: flamegraph_data, version });
                }
            });
            
            || {}
        }, version.clone());
    }

    match &*flamegraph_data {
        FlamegraphDataState::Loading => {
            html!({"loading flamegraph"})
        },
        FlamegraphDataState::Loaded { flamegraph, .. } => {
            let path: Vec<String> = vec!["root".to_owned()];
            let selected_path_setter = selected_path.setter().clone();
            let selected_path = (*selected_path).clone();
            let search_query_setter = search_query.setter().clone();
            let search_query = (*search_query).clone();

            html! {
                <>
                    <input onchange={
                        Callback::from(move |e: Event| {
                            let input: HtmlInputElement = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                            let search_query_input = input.value();
                            search_query_setter.set(search_query_input);
                        })
                    } type="text" placeholder="Search..." />
                    <FlamegraphNode 
                        layer={0} 
                        path={path}
                        selected_path={selected_path}
                        data={flamegraph.root.clone()}
                        search_query={search_query}
                        onselect={Callback::from(move |path: Vec<String>| {
                            selected_path_setter.set(path.clone());
                        })} />
                    <div style="width: 400px; margin: 20px 0 0 auto; padding: 12px; background-color: #f5f6fa; border-radius: 6px; font-weight: 100; font-size: 14px; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24); user-select: none;">
                        {
                            [
                                ("standard library", STANDARD_LIBRARY_COLOR), 
                                ("external dependency", EXTERNAL_DEPENDENCY_COLOR), 
                                ("application", APPLICATION_COLOR), 
                                ("unknown", UNKNOWN_COLOR)
                            ]
                                .iter()
                                .map(|(name, color)| {
                                    html! {
                                        <div style="display: flex; align-items: center;">
                                            <div style={format!("width: 12px; height: 12px; border-radius: 6px; background-color: {}; margin-right: 8px;", color)}></div>
                                            <p>{name}</p>
                                        </div>
                                    }
                                })
                                .collect::<Vec<VNode>>()
                        }
                        <div class="legend-row">
                            <div class="legend-entry"></div>
                            <div class="legend-explanation"></div>
                        </div>
                    </div>
                </>
            }
        },
    }
}

#[function_component(FlamegraphNode)]
pub fn flamegraph_node(props: &FlamegraphNodeProps) -> Html {
    let in_path = props.selected_path.starts_with(&props.path);

    let children = if props.layer < 128 {
        let in_path_child = props.data.children.iter()
            .find(|(key, _child_node)| {
                let mut path = props.path.clone();
                path.push((*key).clone());

                props.selected_path.starts_with(&path)
            });

        if let Some((key, in_path_child)) = in_path_child {
            let mut path = props.path.clone();
            path.push(key.clone());

            html!{
                <FlamegraphNode 
                    layer={props.layer + 1} 
                    data={in_path_child.clone()} 
                    parent_frames={props.data.count} 
                    path={path} 
                    selected_path={props.selected_path.clone()}
                    search_query={props.search_query.clone()}
                    onselect={props.onselect.clone()} />
            }
        } else {
            let mut children: Vec<(String, FlamegraphNodeData)> = props.data.children.iter()
                .map(|(key, value)| (key.clone(), value.clone()))
                .collect();
            children.sort_by(|a, b| a.0.cmp(&b.0));
            
            let child_nodes: Vec<VNode> = children.iter()
            .filter(|(_key, child_node)| (child_node.count as f64 / props.data.count as f64) > 0.01)
            .map(|(key, child_node)| {
                let mut path = props.path.clone();
                path.push(key.clone());

                html!{
                    <FlamegraphNode 
                        layer={props.layer + 1} 
                        data={child_node.clone()} 
                        parent_frames={props.data.count} 
                        path={path} 
                        selected_path={props.selected_path.clone()}
                        search_query={props.search_query.clone()}
                        onselect={props.onselect.clone()} />
                }
            })
            .collect();

            html! { <> { child_nodes } </> }
        }
    } else {
        html! {}
    };

    let name = props.data.name.clone();
    let name = if name == "" {
        "root".to_owned()
    } else {
        name
    };

    let percentage = if in_path {
        100.0
    } else {
        if let Some(parent_frames) = props.parent_frames {
            (props.data.count as f64) / (parent_frames as f64) * 100.0
        } else {
            100.0
        }
    };

    let background_color = if let Some(origin) = &props.data.function_origin {
        match origin {
            &FunctionOrigin::StandardLibrary => STANDARD_LIBRARY_COLOR.to_owned(),
            &FunctionOrigin::ExternalDependency => EXTERNAL_DEPENDENCY_COLOR.to_owned(),
            &FunctionOrigin::Application => APPLICATION_COLOR.to_owned(),
        }
    } else {
        UNKNOWN_COLOR.to_owned()
    };

    html! {
        <div style={format!("width: {}%", percentage)}>
            <div style={format!(r#"
                background-color: {};
                color: white;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-size: 12px;
                font-weight: 100;
                border-bottom: 1px solid white;
                border-right: 1px solid white;
                user-select: none;
                cursor: pointer;
            "#, background_color)} onclick={{
                let path = props.path.clone();
                let onselect = props.onselect.clone();

                Callback::from(move |_| {
                    onselect.clone().emit(path.clone());
                })
            }}>{ name }</div>
            <div style=r#"display: flex;"#>
                { children }
            </div>
        </div>
    }
}