#![recursion_limit = "256"]

use {
    std::collections::HashSet,
    yew::{prelude::*, virtual_dom::VNode},
    yew_router::prelude::*,
    log::info,
    web_sys::HtmlInputElement,
    stdweb::js,
    wasm_bindgen::{prelude::*, JsCast},
    gloo_net::http::Request,
    wasm_bindgen_futures::spawn_local,
    profiling_core::protocol::{
        ListServicesResponse,
        BasicServiceDetails,
        ListVersionsResponse,
    },
    crate::flamegraph::Flamegraph,
};

mod flamegraph;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/service/:service")]
    Service {
        service: String,
    },
}

enum ServicesListState {
    Loading,
    Loaded {
        services: ListServicesResponse,
    }
}

#[derive(Debug)]
enum ServiceVersionsState {
    Loading,
    Loaded {
        versions: Vec<ServiceVersionOption>,
    }
}

#[derive(Clone, Debug)]
struct ServiceVersionOption {
    name: String,
    total_frames: Option<u32>,
}

#[derive(Properties, Clone, PartialEq)]
struct ServiceCardProps {
    service: BasicServiceDetails,
}

#[derive(Properties, Clone, PartialEq)]
struct ServicePageProps {
    service: String,
}

#[derive(Properties, Clone, PartialEq)]
struct HeaderProps {
    service: Option<String>,
}

#[function_component(App)]
fn app() -> Html {
    html!(
        <BrowserRouter>
            <Switch<Route> render={router_switch} />
        </BrowserRouter>
    )
}

#[function_component(Home)]
fn home() -> Html {
    let services = use_state(|| ServicesListState::Loading);

    {
        let setter = services.setter().clone();
        let is_loading = match *services {
            ServicesListState::Loading => true,
            _ => false,
        };

        use_effect(move || {
            spawn_local(async move {
                if is_loading {
                    let services_list: ListServicesResponse = Request::get("/api/v1/services")
                        .send()
                        .await
                        .unwrap()
                        .json()
                        .await
                        .expect("failed to parse list services response");
    
                    setter.set(ServicesListState::Loaded { services: services_list });
                }
            });
    
            || {}
        });
    }
    

    let service_cards = match &*services {
        ServicesListState::Loading => {
            html! {
                <p>{"Loading..."}</p>
            }
        },
        ServicesListState::Loaded { services } => {
            let mut services = services.services.clone();
            services.sort_by(|a, b| {
                let a_total_frames: u32 = a.recent_versions.versions.iter().map(|v| v.total_frames).sum();
                let b_total_frames: u32 = b.recent_versions.versions.iter().map(|v| v.total_frames).sum();
                b_total_frames.cmp(&a_total_frames)
            });

            let services: Vec<VNode> = services.iter()
                .map(|service| html!{
                    <ServiceCard service={service.clone()} />
                })
                .collect();

            html! {
                <p>{services}</p>
            }
        }
    };

    html!(
        <>
            <Header />
            <main style=r#"
                margin: 16px auto 0 auto;
                width: 800px;
            "#>
                <h1 style="user-select: none; text-align: center;">{"Services"}</h1>
                { service_cards }
            </main>
        </>
    )
}

#[function_component(ServiceCard)]
fn service_card(props: &ServiceCardProps) -> Html {
    let navigator = use_navigator().unwrap();

    let service_name = props.service.name.clone();

    let mut versions = props.service.recent_versions.versions.clone();
    versions.sort_by(|a, b| b.total_frames.cmp(&a.total_frames));

    let mut activity = props.service.activity.buckets.clone();
    activity.sort_by(|a, b| a.timestamp.cmp(&b.timestamp));
    let activity: Vec<u32> = activity.into_iter()
        .map(|v| v.total_frames)
        .collect();

    let versions: Vec<VNode> = versions[0..versions.len().min(5)].iter()
        .map(|version| {
            html!{
                <p style=r#"font-size: 14px; font-weight: 100; margin-right: 8px;"#>{ &version.version }</p>
            }
        })
        .collect();

    {
        let service_name = service_name.clone();

        use_effect(move || {
            draw_chart(&service_name, activity);
        
            || {}
        });
    }

    html!(
        <div style=r#"
            background-color: #f5f6fa;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            padding: 12px 20px;
            margin: 20px auto 0 auto;
            width: 500px;
            border-radius: 4px;
            display: flex;
            cursor: pointer;
            user-select: none;
        "# onclick={{
            let service_name = service_name.clone();
            let navigator = navigator.clone();

            Callback::from(move |_| {
                navigator.push(&Route::Service { service: service_name.clone() });
            })
        }}>
            <div style=r#"width: 400px;"#>
                <p style=r#"font-size: 20px;"#>{ &props.service.name }</p>
                <div style=r#"display: flex;"#>
                    { versions }
                </div>
            </div>
            <canvas id={format!("activity-{}", service_name)} style=r#"width: 100px; height: 50px;"#></canvas>
        </div>
    )
}

#[function_component(ServicePage)]
fn service_page(props: &ServicePageProps) -> Html {
    let version_options = use_state(|| ServiceVersionsState::Loading);
    let all_versions = use_state(|| HashSet::<String>::new());
    let invalid_version_selected = use_state(|| false);
    let selected_version: UseStateHandle<Option<String>> = use_state(|| None);

    {
        let version_options_setter = version_options.setter().clone();
        let all_versions_setter = all_versions.setter().clone();
        let service_name = props.service.clone();

        let is_loading = match *version_options {
            ServiceVersionsState::Loading => true,
            _ => false,
        };

        use_effect(move || {
            spawn_local(async move {
                if is_loading {
                    let versions_list: ListVersionsResponse = Request::get(&format!("/api/v1/services/{}/versions", service_name))
                        .send()
                        .await
                        .unwrap()
                        .json()
                        .await
                        .expect("failed to parse list versions response");
    
                    all_versions_setter.set(versions_list.all_versions.iter().cloned().collect());

                    let mut versions_list: Vec<ServiceVersionOption> = versions_list.recent_versions.versions.into_iter()
                        .map(|version| ServiceVersionOption {
                            name: version.version.clone(),
                            total_frames: Some(version.total_frames),
                        })
                        .collect();

                    versions_list.sort_by(|a, b| b.total_frames.cmp(&a.total_frames));

                    version_options_setter.set(ServiceVersionsState::Loaded { versions: versions_list });
                }
            });
    
            || {}
        });
    }

    let versions_list = match &*version_options {
        ServiceVersionsState::Loading => html!{},
        ServiceVersionsState::Loaded { versions } => {
            let versions: Vec<VNode> = (*versions).iter().map(|version| {
                let version = version.clone();
                let class_name = if let Some(selected_version) = &*selected_version {
                    if selected_version == &version.name {
                        "version-selector version-selector-selected"
                    } else {
                        "version-selector"
                    }
                } else { 
                    "version-selector"
                };

                html!{
                    <p class={class_name} onclick={{
                        let version_name = version.name.clone();
                        let selected_version_setter = selected_version.setter();

                        Callback::from(move |_| {
                            selected_version_setter.set(Some(version_name.clone()));
                        })
                    }}>{version.name.clone()}</p>
                }
            }).collect();

            html!{ 
                <>{ versions }</>
            }
        }
    };

    let flamegraph = if let Some(selected_version) = &*selected_version {
        html!(
            <Flamegraph service={props.service.clone()} version={selected_version.clone()} />
        )
    } else {
        html!()
    };

    html!(
        <>
            <Header service={Some(props.service.clone())} />
            <div style=r#"
                padding: 14px;
            "#>
                <h1>{"Flamegraph"}</h1>
                <div style=r#"margin-top: 14px; display: flex"#>
                    <input onchange={
                        let versions_setter = version_options.setter();
                        let selected_version_setter = selected_version.setter();
                        let invalid_version_selected_setter = invalid_version_selected.setter();

                        Callback::from(move |e: Event| {
                            let input: HtmlInputElement = e.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
                            let selected_version = input.value();
                            
                            let selected_version_exists = all_versions.contains(&selected_version);

                            if selected_version_exists {
                                let updated_service_options = match &* version_options {
                                    ServiceVersionsState::Loading => ServiceVersionsState::Loading,
                                    ServiceVersionsState::Loaded { versions } => {
                                        let mut versions = versions.clone();
    
                                        let version_already_exists = versions.iter().any(|version| version.name == selected_version);
                                        if !version_already_exists {
                                            versions.insert(0, ServiceVersionOption {
                                                name: selected_version.clone(),
                                                total_frames: None,
                                            });
                                        }
                                        
                                        ServiceVersionsState::Loaded { versions }
                                    }
                                };
    
                                versions_setter.set(updated_service_options);
                                selected_version_setter.set(Some(selected_version));
                                invalid_version_selected_setter.set(false);
                                input.set_value("");
                            } else {
                                invalid_version_selected_setter.set(true);
                            }
                        })
                    } class={if *invalid_version_selected { "invalid-input" } else { "" }} type="text" placeholder="version..." style="width: 200px;" />
                    { versions_list }
                </div>
                <div style="margin-top: 14px;">{flamegraph}</div>
            </div>
        </>
    )
}

#[function_component(Header)]
fn header(props: &HeaderProps) -> Html {
    let navigator = use_navigator().unwrap();

    let service_name = match &props.service {
        Some(service) => html!{
            <p style=r#"
                display: inline;
                margin-left: 10px;
                color: #7f8fa6;
            "#>{ format!("| {}", service) }</p>
        },
        None => html!{}
    };

    html!(
        <header style=r#"
            background-color: #2f3640;
            color: white;
            padding: 16px;
            user-select: none;
            cursor: pointer;
        "# onclick={Callback::from(move |_| {
            navigator.push(&Route::Home);
        })}>
            {"performance dashboard"}
            { service_name }
        </header>
    )
}

fn router_switch(routes: Route) -> Html {
    match routes {
        Route::Home => html!(<Home />),
        Route::Service { service } => html!(<ServicePage service={service} />),
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::Renderer::<App>::new().render();
}

#[wasm_bindgen]
extern "C" {
    fn draw_chart(service_name: &str, activity: Vec<u32>);
}