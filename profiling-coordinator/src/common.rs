use std::env;

pub const KAFKA_TOPIC_COLLECTED_PROFILES: &str = "collected-profiles";

pub fn kafka_broker_host() -> String {
    env::var("BROKER_HOST").unwrap_or("127.0.0.1:9092".to_owned())
}