use {
    std::{
        thread, 
        time::Duration, 
        collections::HashMap, 
        io::Cursor,
        sync::{Arc, Mutex},
        env,
        time::SystemTime,
    },
    actix_web::{App, HttpServer, Responder, get, post, HttpRequest, HttpResponse, web::{self, Bytes, PayloadConfig}},
    async_trait::async_trait,
    async_stream::stream,
    kafka::producer::{Producer, Record, RequiredAcks},
    env_logger::Env,
    log::info,
    rsocket_rust::prelude::*,
    rsocket_rust_transport_tcp::TcpServerTransport,
    redis::Commands,
    rand::Rng,
    serde::{Serialize, Deserialize},
    s3::{Region, bucket::Bucket, creds::Credentials},
    argon2::{password_hash::{rand_core::OsRng, SaltString}, Argon2, PasswordHasher, PasswordVerifier, PasswordHash},
    thiserror::Error,
    profiling_core::{
        protocol::{
            REDIS_KEY_PREFIX,
            KAFKA_TOPIC_SERVICE_BINARIES,
            KAFKA_TOPIC_COLLECTED_PERF_COUNTERS,
            CollectedProfile,
            CollectedProfileWithMetadata,
            ProfilingSessionInit, 
            ProfilingSessionAgentToCoordinatorMessage,
            ProfilingSessionCoordinatorToAgentMessage,
            ProfilerTask,
            ServiceBinaryUpdateEvent,
            AgentMetadata,
        },
        perf_counters::PerfCounterData,
    },
    crate::{
        common::{KAFKA_TOPIC_COLLECTED_PROFILES, kafka_broker_host},
        config::{ContinuousProfilerConfig, ServiceConfig},
    }
};

mod common;
mod config;

struct ProfilingCoordinatorRSocket {
    config: ServiceConfig,
    producer: Arc<Mutex<Producer>>,
    agent_id: String,
    service_name: String,
    service_version: String,
    tags: HashMap<String, String>,
}

#[derive(Serialize, Deserialize)]
struct ActiveProfilingAgents {
    agents: Vec<ActiveProfilingAgent>,
}

#[derive(Serialize, Deserialize)]
struct ActiveProfilingAgent {
    agent_id: String,
    last_contact_at: SystemTime,
}

#[derive(Deserialize)]
struct ServiceVersionPath {
    service: String,
    version: String,
}

#[derive(Serialize)]
struct AccessTokenAndHash {
    token: String,
    hash: String,
    hash_encoded: String,
}

#[derive(Error, Debug)]
enum AuthError {
    #[error("unknown access token")]
    UnknownAccessToken,
    #[error("other auth error")]
    Other,
}

const MAX_FILE_SIZE: usize = 500 * 1024 * 1024;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    info!("starting continuous profiler server...");

    let config = config::read_config();

    let producer = Arc::new(Mutex::new(Producer::from_hosts(vec![kafka_broker_host()])
        .with_ack_timeout(Duration::from_secs(10))
        .with_required_acks(RequiredAcks::One)
        .create()
        .unwrap()));

    run_profiler_coorindator_rsocket_server(config.clone(), producer.clone());

    let config = web::Data::new(config);
    let producer = web::Data::new(producer);
    let storage = web::Data::new(Arc::new(Bucket::new(
        "nikitavbv-continuous-profiling",
        Region::Custom {
            region: "europe-central2".to_owned(),
            endpoint: "https://storage.googleapis.com".to_owned(),
        },
        Credentials::from_env_specific(
            Some("STORAGE_ACCESS_KEY_ID"),
            Some("STORAGE_SECRET_ACCESS_KEY"),
            None,
            None,
        ).unwrap()
    ).unwrap()));

    HttpServer::new(move || App::new()
        .app_data(PayloadConfig::default().limit(MAX_FILE_SIZE))
        .app_data(config.clone())
        .app_data(storage.clone())
        .app_data(producer.clone())
        .service(index)
        .service(upload_service_binary)
        .service(private_recalculate_aggregations)
        .service(private_generate_access_token)
        .service(private_profiling_configuraion_for_service)
    ).bind(("0.0.0.0", 8080))?
        .run()
        .await
}

#[get("/")]
async fn index() -> impl Responder {
    "ok"
}

#[post("/api/v1/service/{service}/versions/{version}/binary")]
async fn upload_service_binary(req: HttpRequest, config: web::Data<ContinuousProfilerConfig>, storage: web::Data<Arc<Bucket>>, producer: web::Data<Arc<Mutex<Producer>>>, params: web::Path<ServiceVersionPath>, binary: Bytes) -> impl Responder {
    if let Err(err) = check_auth(&config, req) {
        info!("auth failed: {:?}", err);
        return HttpResponse::Unauthorized().body("unauthorized");
    }
    
    info!("uploading service binary for service {} version {}", params.service, params.version);
    storage.put_object(format!("services/{}/versions/{}/binary", params.service, params.version), &binary).await.unwrap();
    
    producer.lock().unwrap().send(&Record::from_value(KAFKA_TOPIC_SERVICE_BINARIES, serde_json::to_vec(&ServiceBinaryUpdateEvent {
        service: params.service.clone(),
        version: params.version.clone(),
    }).unwrap())).unwrap();

    info!("done uploading service binary for service {} version {}", params.service, params.version);
    HttpResponse::Ok().body("ok")
}

#[post("/private/v1/maintenance/recalculate-aggregations")]
async fn private_recalculate_aggregations() -> impl Responder {
    info!("recalculating aggregations");

    info!("performing redis cleanup...");
    let redis_client = redis::Client::open(redis_connection_string()).unwrap();
    let mut redis_connection = redis_client.get_connection().unwrap();

    let keys_to_cleanup: Vec<String> = redis_connection.scan_match::<String, String>(format!("{}*", REDIS_KEY_PREFIX)).unwrap().collect();
    for key in keys_to_cleanup {
        info!("deleting redis key: {}", key);
        redis_connection.del::<String, ()>(key).unwrap();
    }
    info!("redis cleanup completed");

    HttpResponse::Ok().body("ok")
}

#[post("/private/v1/maintenance/generate-token")]
async fn private_generate_access_token() -> impl Responder {
    let random_token: Vec<u8> = (0..64).map(|_| { rand::random::<u8>() }).collect();
    let encoded_token: String = base64::encode(&random_token);

    let salt = SaltString::generate(&mut OsRng);
    let token_hash = Argon2::default().hash_password(&random_token, &salt).unwrap();

    HttpResponse::Ok().json(AccessTokenAndHash {
        token: encoded_token,
        hash: token_hash.to_string(),
        hash_encoded: base64::encode(&token_hash.to_string().as_bytes()),
    })
}

#[get("/private/v1/service/{service}/config")]
async fn private_profiling_configuraion_for_service(config: web::Data<ContinuousProfilerConfig>, service: web::Path<String>) -> impl Responder {
    match config.config_for_service(&service) {
        Some(v) => HttpResponse::Ok().json(v),
        None => HttpResponse::NotFound().body("not_found"),
    }
}

fn run_profiler_coorindator_rsocket_server(config: ContinuousProfilerConfig, producer: Arc<Mutex<Producer>>) {
    thread::spawn(move || {
        let rt = actix_rt::Runtime::new().unwrap();

        rt.block_on(async move {
            info!("starting rsocket profiling coordinator server");

            RSocketFactory::receive()
                .transport(TcpServerTransport::from("0.0.0.0:7878"))
                .acceptor(Box::new(move |setup, _socket| {
                    let init_msg: ProfilingSessionInit = serde_json::from_slice(&setup.data().unwrap()).unwrap();

                    let config = match config.config_for_service(&init_msg.service_name) {
                        Some(v) => v.clone(),
                        None => ServiceConfig::default(init_msg.service_name.clone()),
                    };

                    info!("token config for {}: {:?}, provided token is {:?}", &init_msg.service_name, config.access_token(), init_msg.access_token);

                    Ok(Box::new(ProfilingCoordinatorRSocket::new(config, producer.clone(), init_msg.agent_id, init_msg.service_name, init_msg.service_version, init_msg.tags)))
                }))
                .on_start(Box::new(|| info!("rsocket profiling coordinator server is running!")))
                .serve()
                .await
                .unwrap();
        });
    });
}

impl ProfilingCoordinatorRSocket {
    fn new(config: ServiceConfig, producer: Arc<Mutex<Producer>>, agent_id: String, service_name: String, service_version: String, tags: HashMap<String, String>) -> Self {
        Self {
            config,
            producer,
            agent_id,
            service_name,
            service_version,
            tags,
        }
    }
}

// see https://github.com/rsocket/rsocket-rust/blob/8dddb71f87112bcfdd7b3161b2a51a4cd0e3575a/rsocket/src/utils.rs
#[async_trait]
impl RSocket for ProfilingCoordinatorRSocket {
    async fn request_response(&self, req: Payload) -> rsocket_rust::Result<Option<Payload>> {
        let response = match serde_json::from_slice::<ProfilingSessionAgentToCoordinatorMessage>(&req.data().unwrap()).unwrap() {
            ProfilingSessionAgentToCoordinatorMessage::RequestProfilingTask => {
                let task = if rand::thread_rng().gen_bool(self.config.profiling_rate()) {
                    ProfilingSessionCoordinatorToAgentMessage::RunProfiler(ProfilerTask {
                        run_for: self.config.profiling_duration(),
                        frequency: self.config.profiling_frequency(),
                    })
                } else {
                    ProfilingSessionCoordinatorToAgentMessage::DoNothing
                };

                info!("profiling task requested by agent = {} for service = {} version = {}, replying with = {:?}", self.agent_id, self.service_name, self.service_version, task);

                task
            },
            ProfilingSessionAgentToCoordinatorMessage::SubmitCollectedProfileCompressed(collected_profile_compressed) => {
                let collected_profile_json = zstd::stream::decode_all(Cursor::new(collected_profile_compressed)).unwrap();
                let collected_profile: CollectedProfile = serde_json::from_slice(&collected_profile_json).unwrap();
                let collected_profile_with_metadata: CollectedProfileWithMetadata = CollectedProfileWithMetadata {
                    version: 1,
                    collected_profile,
                    agent_id: self.agent_id.clone(),
                    service_name: self.service_name.clone(),
                    service_version: self.service_version.clone(),
                    tags: self.tags.clone(),
                };
                let with_metadata_json = serde_json::to_vec(&collected_profile_with_metadata).unwrap();
                let with_metadata_compressed = zstd::stream::encode_all(Cursor::new(with_metadata_json), 10).unwrap();

                self.producer.lock().unwrap().send(&Record::from_value(KAFKA_TOPIC_COLLECTED_PROFILES, with_metadata_compressed)).unwrap();

                ProfilingSessionCoordinatorToAgentMessage::DoNothing
            },
            ProfilingSessionAgentToCoordinatorMessage::SubmitCollectedPerfCounters(perf_counters) => {
                let perf_counters_with_metadata: (PerfCounterData, AgentMetadata) = (perf_counters, AgentMetadata {
                    version: 1,
                    agent_id: self.agent_id.clone(),
                    service_name: self.service_name.clone(),
                    service_version: self.service_version.clone(),
                    tags: self.tags.clone(),
                });
                let with_metadata_json = serde_json::to_vec(&perf_counters_with_metadata).unwrap();

                self.producer.lock().unwrap().send(&Record::from_value(KAFKA_TOPIC_COLLECTED_PERF_COUNTERS, with_metadata_json)).unwrap();

                ProfilingSessionCoordinatorToAgentMessage::DoNothing
            }
        };

        Ok(Some(Payload::builder()
            .set_data(serde_json::to_vec(&response).unwrap())
            .build()
        ))
    }

    async fn metadata_push(&self, req: Payload) -> rsocket_rust::Result<()> {
        info!("metadata push: {:?}", req);
        Ok(())
    }

    async fn fire_and_forget(&self, req: Payload) -> rsocket_rust::Result<()> {
        info!("fire and forget: {:?}", req);
        Ok(())
    }

    fn request_stream(&self, req: Payload) -> Flux<rsocket_rust::Result<Payload>> {
        info!("request stream: {:?}", req);
        Box::pin(stream!{
            yield Ok(req);
        })
    }

    fn request_channel(&self, mut req: Flux<rsocket_rust::Result<Payload>>) -> Flux<rsocket_rust::Result<Payload>> {
        info!("request channel");
        Box::pin(stream!{
            while let Some(it) = req.next().await {
                yield it;
            }
        })
    }
}

fn check_auth(config: &ContinuousProfilerConfig, req: HttpRequest) -> Result<(), AuthError> {
    let token = req.headers().get("Authorization")
        .ok_or(AuthError::Other)?
        .to_str().unwrap()
        .trim_start_matches("Bearer ");
    let token_bytes = base64::decode(&token).unwrap();

    for token in config.tokens() {
        if let Some(token) = token.token() {
            if let Ok(hash) = PasswordHash::new(&token) {
                if Argon2::default().verify_password(&token_bytes, &hash).is_ok() {
                    return Ok(())
                }
            }
        }
    }

    Err(AuthError::UnknownAccessToken)
}

pub fn redis_connection_string() -> String {
    env::var("REDIS_CONNECTION_STRING").unwrap_or("redis://127.0.0.1:6379/".to_owned())
}