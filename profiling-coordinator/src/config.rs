use {
    std::{fs::read_to_string, collections::HashMap, time::Duration, env},
    log::{info, error},
    serde::{Serialize, Deserialize},
    yaml_rust::{Yaml, YamlLoader, YamlEmitter},
};

const PROFILING_CONFIG_DIR: &str = "config";

#[derive(Clone)]
pub struct ContinuousProfilerConfig {
    tokens: Vec<TokenConfig>,
    services: HashMap<String, ServiceConfig>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct TokenConfig {
    pub env: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ServiceConfig {
    service: String,
    #[serde(default = "default_profiling_rate")]
    profiling_rate: f64,
    #[serde(default = "default_profiling_frequency")]
    profiling_frequency: u32,
    #[serde(default = "default_profiling_duration")]
    profiling_duration: String,
    access_token: Option<ServiceAccessTokenConfig>,
    export_metrics: Option<Vec<ExportMetricsConfig>>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ServiceAccessTokenConfig {
    env: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ExportMetricsConfig {
    name: String,
}

impl ContinuousProfilerConfig {
    pub fn tokens(&self) -> &Vec<TokenConfig> {
        &self.tokens
    }

    pub fn config_for_service(&self, service: &str) -> Option<&ServiceConfig> {
        self.services.get(service)
    }
}

impl TokenConfig {
    pub fn token(&self) -> Option<String> {
        env::var(&self.env).ok()
    }
}

impl ServiceConfig {
    pub fn default(service: String) -> Self {
        Self {
            service,
            profiling_rate: default_profiling_rate(),
            profiling_frequency: default_profiling_frequency(),
            profiling_duration: default_profiling_duration(),
            access_token: None,
            export_metrics: None,
        }
    }

    pub fn profiling_rate(&self) -> f64 {
        self.profiling_rate
    }

    pub fn profiling_frequency(&self) -> u32 {
        self.profiling_frequency
    }

    pub fn profiling_duration(&self) -> Duration {
        parse_duration::parse(&self.profiling_duration).unwrap()
    }

    pub fn access_token(&self) -> Option<String> {
        self.access_token.as_ref().and_then(|token_config| token_config.resolve())
    }
}

impl ServiceAccessTokenConfig {
    fn resolve(&self) -> Option<String> {
        env::var(&self.env).ok()
    }
}

fn default_profiling_rate() -> f64 {
    1.0
}

fn default_profiling_frequency() -> u32 {
    1000
}

fn default_profiling_duration() -> String {
    "10s".to_owned()
}

pub fn read_config() -> ContinuousProfilerConfig {
    info!("reading config...");
    ContinuousProfilerConfig {
        tokens: load_tokens_config(),
        services: load_services_config(),
    }
}

fn load_tokens_config() -> Vec<TokenConfig> {
    let config = match read_to_string("config/tokens.yaml") {
        Ok(v) => v,
        Err(err) => {
            error!("failed to read tokens config file: {:?}", err);
            return Vec::new();
        }
    };

    YamlLoader::load_from_str(&config).unwrap()
        .iter()
        .map(|v| parse_token_config(v))
        .collect()
}

fn load_services_config() -> HashMap<String, ServiceConfig> {
    let config = match read_to_string("config/profiling.yaml") {
        Ok(v) => v,
        Err(err) => {
            error!("failed to read services config file: {:}, profiling dir contents: {:?}", err, std::fs::read_dir(PROFILING_CONFIG_DIR).unwrap());
            return HashMap::new();
        }
    };
    
    YamlLoader::load_from_str(&config).unwrap()
        .iter()
        .map(|v| parse_service_config(v))
        .map(|v| (v.service.clone(), v))
        .collect()
}

fn parse_token_config(config: &Yaml) -> TokenConfig {
    let mut serialized = String::new();
    {
        let mut emitter = YamlEmitter::new(&mut serialized);
        emitter.dump(config).unwrap();
    }

    serde_yaml::from_str(&serialized).unwrap()
}

fn parse_service_config(config: &Yaml) -> ServiceConfig {
    let mut serialized = String::new();
    {
        let mut emitter = YamlEmitter::new(&mut serialized);
        emitter.dump(config).unwrap();
    }
    
    serde_yaml::from_str(&serialized).unwrap()
}